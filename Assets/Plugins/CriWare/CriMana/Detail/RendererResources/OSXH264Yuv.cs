﻿/****************************************************************************
 *
 * Copyright (c) 2015 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
using UnityEngine;
using System.Runtime.InteropServices;
namespace CriMana.Detail
{
	public static partial class AutoResisterRendererResourceFactories
	{
		[RendererResourceFactoryPriority(7000)]
		public class RendererResourceFactoryOSXH264Yuv : RendererResourceFactory
		{
			public override RendererResource CreateRendererResource(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
			{
				bool isCodecSuitable = movieInfo.codecType == CodecType.H264;
				bool isSuitable      = isCodecSuitable;
				return isSuitable
					? new RendererResourceOSXH264Yuv(playerId, movieInfo, additive, userShader)
					: null;
			}
			
			protected override void OnDisposeManaged()
			{
			}
			
			protected override void OnDisposeUnmanaged()
			{
			}
		}
	}

	public class RendererResourceOSXH264Yuv : RendererResource
	{
		private int		width;
		private int		height;
		private int 	playerId;
		private bool	hasAlpha;
		private bool	additive;
		private bool	applyTargetAlpha;
		private bool	useUserShader;
		private Shader			shader;
		private Vector4			movieTextureST = Vector4.zero;
		private Texture2D		textureRGB;
		private Texture2D		textureA;
		System.IntPtr[]			nativePtrs;
		private Material currentMaterial = null;
		private int srcBlendMode;
		private int dstBlendMode;


		public RendererResourceOSXH264Yuv(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
		{
			this.width		= (int)movieInfo.width;
			this.height		= (int)movieInfo.height;
			this.playerId	= playerId;
			this.hasAlpha	= movieInfo.hasAlpha;
			this.additive	= additive;
			this.useUserShader	= userShader != null;
			
			if (userShader != null) {
				shader = userShader;
			} else {
				string shaderName = "CriMana/OSXH264Yuv";
				shader = Shader.Find(shaderName);
			}

			if (hasAlpha) {
				srcBlendMode = additive ? (int)UnityEngine.Rendering.BlendMode.One : (int)UnityEngine.Rendering.BlendMode.SrcAlpha;
				dstBlendMode = (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha;
				nativePtrs = new System.IntPtr[2];
			} else {
				srcBlendMode = (int)UnityEngine.Rendering.BlendMode.One;
				dstBlendMode = additive ? (int)UnityEngine.Rendering.BlendMode.One : (int)UnityEngine.Rendering.BlendMode.Zero;
				nativePtrs = new System.IntPtr[1];
			}
		}

		protected override void OnDisposeManaged()
		{
		}
		
		protected override void OnDisposeUnmanaged()
		{
			if (textureRGB != null) {
				Texture2D.Destroy(textureRGB);
				textureRGB = null;
			}
			if (textureA != null) {
				Texture2D.Destroy(textureA);
				textureA = null;
			}
			currentMaterial = null;
		}
		
		public override bool IsPrepared()
		{ return true; }
		
		public override bool ContinuePreparing()
		{ return true; }
		
		public override bool IsSuitable(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
		{
			bool isCodecSuitable    = movieInfo.codecType == CodecType.H264;
			bool isAlphaSuitable    = hasAlpha == movieInfo.hasAlpha;
			bool isAdditiveSuitable = this.additive == additive;
			bool isShaderSuitable   = this.useUserShader ? (userShader == shader) : true;
			return isCodecSuitable && isAlphaSuitable && isAdditiveSuitable && isShaderSuitable;
		}

 		public override void SetApplyTargetAlpha(bool flag)
		{
			applyTargetAlpha = flag;

			if (hasAlpha || applyTargetAlpha) {
				srcBlendMode = additive ? (int)UnityEngine.Rendering.BlendMode.One : (int)UnityEngine.Rendering.BlendMode.SrcAlpha;
				dstBlendMode = (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha;
			} else {
				srcBlendMode = (int)UnityEngine.Rendering.BlendMode.One;
				dstBlendMode = additive ? (int)UnityEngine.Rendering.BlendMode.One : (int)UnityEngine.Rendering.BlendMode.Zero;
			}

			if (currentMaterial != null) {
				currentMaterial.SetInt("_SrcBlendMode", srcBlendMode);
				currentMaterial.SetInt("_DstBlendMode", dstBlendMode);
				if (applyTargetAlpha) {
					currentMaterial.EnableKeyword("CRI_APPLY_TARGET_ALPHA");
				} else {
					currentMaterial.DisableKeyword("CRI_APPLY_TARGET_ALPHA");
				}
			}
		}

		public override void AttachToPlayer(int playerId)
		{
			// reset texture if exist
			OnDisposeUnmanaged();
		}

		public override bool UpdateFrame(int playerId, FrameInfo frameInfo)
		{
			bool isFrameUpdated = criManaUnityPlayer_UpdateFrame(playerId, 0, null, frameInfo);
			if (isFrameUpdated) {
				UpdateMovieTextureST(frameInfo.dispWidth, frameInfo.dispHeight);
			}

			return isFrameUpdated;
		}

		public override bool UpdateMaterial(Material material)
		{
			if (currentMaterial != material) {
				if (textureRGB != null) {
					if (material.shader != shader) {
						material.shader = shader;
					}
					material.SetTexture("_TextureRGB", textureRGB);
					material.SetInt("_SrcBlendMode", srcBlendMode);
					material.SetInt("_DstBlendMode", dstBlendMode);
					if (hasAlpha) {
						material.EnableKeyword("CRI_ALPHA_MOVIE");
						material.SetTexture("_TextureA", textureA);
					}
					if (applyTargetAlpha) {
						material.EnableKeyword("CRI_APPLY_TARGET_ALPHA");
					}
					if (QualitySettings.activeColorSpace == ColorSpace.Linear) {
						material.EnableKeyword("CRI_LINEAR_COLORSPACE");
					}
				} else {
					return false;
				}
				currentMaterial = material;
			}
			material.SetVector("_MovieTexture_ST", movieTextureST);
			return true;
		}

		private void UpdateMovieTextureST(System.UInt32 dispWidth, System.UInt32 dispHeight)
		{
			float uScale = (dispWidth != width) ? (float)(dispWidth - 0.5f) / width : 1.0f;
			float vScale = (dispHeight != height) ? (float)(dispHeight - 0.5f) / height : 1.0f;
			movieTextureST.x = uScale;
			movieTextureST.y = -vScale;
			movieTextureST.z = 0.0f;
			movieTextureST.w = vScale;
		}
			
		public override void UpdateTextures()
		{
			for (int i = 0; i < nativePtrs.Length; i++) {
				nativePtrs[i] = System.IntPtr.Zero;
			}
			bool isTextureUpdated = criManaUnityPlayer_UpdateTextures(playerId, nativePtrs.Length, nativePtrs);
			if (isTextureUpdated && nativePtrs[0] != System.IntPtr.Zero) {
				if (textureRGB == null) {
					textureRGB = Texture2D.CreateExternalTexture(width, height, TextureFormat.BGRA32, false, false, nativePtrs[0]);
					textureRGB.wrapMode = TextureWrapMode.Clamp;
					if (hasAlpha) {
						textureA = Texture2D.CreateExternalTexture(width, height, TextureFormat.R8, false, false, nativePtrs[1]);
						textureA.wrapMode = TextureWrapMode.Clamp;
					}
				} else {
					textureRGB.UpdateExternalTexture(nativePtrs[0]);
					if (hasAlpha) {
						textureA.UpdateExternalTexture(nativePtrs[1]);
					}
				}
			}
		}
	}
}
#endif
