﻿/****************************************************************************
 *
 * Copyright (c) 2017 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_IOS || UNITY_ANDROID || UNITY_PS4 || UNITY_XBOXONE || UNITY_SWITCH
	#define CRIAFX_MCDSP_SUPPORTED
#endif

using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class CriAfxMcDspInitializer : MonoBehaviour
{
	/* スクリプトバージョン */
	private const string scriptVersionString = "1.00.00";
	private const int scriptVersionNumber = 0x01000000;

#if CRIAFX_MCDSP_SUPPORTED

#if UNITY_EDITOR
	public const string pluginName = "criafx_mcdsp";
#elif UNITY_IOS || UNITY_TVOS || UNITY_WEBGL || UNITY_SWITCH
	public const string pluginName = "__Internal";
#else
	public const string pluginName = "criafx_mcdsp";
#endif

	void Awake()
	{
		var dspInterface = criAfxMcDSPAE600_GetInterfaceWithVersion();
		if (dspInterface != IntPtr.Zero) {
			CriWareInitializer.AddAudioEffectInterface(dspInterface);
		}
		dspInterface = criAfxMcDSPFutzBox_GetInterfaceWithVersion();
		if (dspInterface != IntPtr.Zero) {
			CriWareInitializer.AddAudioEffectInterface(dspInterface);
		}
		dspInterface = criAfxMcDSPML4000ML1_GetInterfaceWithVersion();
		if (dspInterface != IntPtr.Zero) {
			CriWareInitializer.AddAudioEffectInterface(dspInterface);
		}
		dspInterface = criAfxMcDSPSA2_GetInterfaceWithVersion();
		if (dspInterface != IntPtr.Zero) {
			CriWareInitializer.AddAudioEffectInterface(dspInterface);
		}
	}

#endif

	/** 
	 * <summary>エフェクトインタフェースの一斉手動登録メソッド</summary>
	 * \attention
	 * 必ずACFファイルの登録前に行ってください。
	 */
	public static void AddInterfaces()
	{
#if CRIAFX_MCDSP_SUPPORTED
		var dspInterface = criAfxMcDSPAE600_GetInterfaceWithVersion();
		if (dspInterface != IntPtr.Zero) {
			CriAtomExAsr.RegisterEffectInterface(dspInterface);
		}
		dspInterface = criAfxMcDSPFutzBox_GetInterfaceWithVersion();
		if (dspInterface != IntPtr.Zero) {
			CriAtomExAsr.RegisterEffectInterface(dspInterface);
		}
		dspInterface = criAfxMcDSPML4000ML1_GetInterfaceWithVersion();
		if (dspInterface != IntPtr.Zero) {
			CriAtomExAsr.RegisterEffectInterface(dspInterface);
		}
		dspInterface = criAfxMcDSPSA2_GetInterfaceWithVersion();
		if (dspInterface != IntPtr.Zero) {
			CriAtomExAsr.RegisterEffectInterface(dspInterface);
		}
#endif
	}

#if CRIAFX_MCDSP_SUPPORTED
	#region DLL Import
	[DllImport(CriAfxMcDspInitializer.pluginName, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern IntPtr criAfxMcDSPAE600_GetInterfaceWithVersion();

	[DllImport(CriAfxMcDspInitializer.pluginName, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern IntPtr criAfxMcDSPFutzBox_GetInterfaceWithVersion();

	[DllImport(CriAfxMcDspInitializer.pluginName, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern IntPtr criAfxMcDSPML4000ML1_GetInterfaceWithVersion();

	[DllImport(CriAfxMcDspInitializer.pluginName, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern IntPtr criAfxMcDSPSA2_GetInterfaceWithVersion();
	#endregion
#endif
}
