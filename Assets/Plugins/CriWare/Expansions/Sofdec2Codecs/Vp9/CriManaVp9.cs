﻿#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_IOS || UNITY_ANDROID
	#define CRIMANA_VP9_SUPPORTED_PLATFORM
#endif

using UnityEngine;
using System.Runtime.InteropServices;
using System.IO;
using System;

/// \addtogroup CriManaVp9
/// @{

/**
 * <summary>ユーザはこのクラスを使用しません</summary>
 */
public static class CriManaVp9
{
	private const string scriptVersionString = "1.00.00";
	private const int scriptVersionNumber = 0x01000000;
	
	/* ネイティブライブラリ */
	#if UNITY_EDITOR
		public const string cri_mana_vp9_name = "cri_mana_vpx";
	#elif UNITY_IOS
		public const string cri_mana_vp9_name = "__Internal";
	#else
		public const string cri_mana_vp9_name = "cri_mana_vpx";
	#endif
	
	static public bool SupportCurrentPlatform()
	{
#if CRIMANA_VP9_SUPPORTED_PLATFORM
		return true;
#else
		return false;
#endif
	}
	
	/* VP9の設定 */
	static public void SetupVp9Decoder()
	{
#if CRIMANA_VP9_SUPPORTED_PLATFORM
		{
			/* VP9ユーザアロケータの設定 */
			IntPtr alloc_func	= criWareUnity_GetAllocateFunc();
			IntPtr free_func	= criWareUnity_GetDeallocateFunc();
			IntPtr usr_obj		= criManaUnity_GetAllocatorManager();
			
			criVvp9_SetUserAllocator(alloc_func, free_func, usr_obj);
		}
		
		{
			/* VP9コーデックインターフェースのアタッチ */
			IntPtr codec_if      = criVvp9_GetInterface();
			IntPtr codecalpha_if = criVvp9_GetAlphaInterface();
			
			criMvPly_AttachCodecInterface((int)CriMana.CodecType.VP9, codec_if, codecalpha_if);
		}
#endif
	}
	
	#region Native API Definition (DLL)
#if CRIMANA_VP9_SUPPORTED_PLATFORM
	[DllImport(CriWare.pluginName, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern IntPtr criWareUnity_GetAllocateFunc();
	[DllImport(CriWare.pluginName, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern IntPtr criWareUnity_GetDeallocateFunc();
	[DllImport(CriWare.pluginName, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern IntPtr criManaUnity_GetAllocatorManager();
	[DllImport(CriWare.pluginName, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern void criMvPly_AttachCodecInterface(int codec_type, IntPtr codec_if, IntPtr codecalpha_if);
	
	[DllImport(cri_mana_vp9_name, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern void criVvp9_SetUserAllocator(IntPtr alloc_func, IntPtr free_func, IntPtr usr_obj);
	[DllImport(cri_mana_vp9_name, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern IntPtr criVvp9_GetInterface();
	[DllImport(cri_mana_vp9_name, CallingConvention = CriWare.pluginCallingConvention)]
	private static extern IntPtr criVvp9_GetAlphaInterface();
#endif
	
	#endregion
}

/// @}
