﻿#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_IOS || UNITY_ANDROID
	#define CRIMANA_VP9_SUPPORTED_PLATFORM
#endif

using UnityEngine;
using System;

#if CRIMANA_VP9_SUPPORTED_PLATFORM

namespace CriMana.Detail
{
	public static partial class AutoResisterRendererResourceFactories
	{
		[RendererResourceFactoryPriority(9000)]
		public class RendererResourceFactoryySofdecVp9Yuv : RendererResourceFactory
		{
			public override RendererResource CreateRendererResource(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
			{
				bool isCodecSuitable = movieInfo.codecType == CodecType.VP9;
				bool isSuitable      = isCodecSuitable;
				
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_IOS
				return isSuitable
					? new RendererResourceSofdecPrimeYuv(playerId, movieInfo, additive, userShader)
					: null;
#elif UNITY_ANDROID
				return isSuitable
					? new RendererResourceAndroidSofdecPrimeYuv(playerId, movieInfo, additive, userShader)
					: null;
#endif
			}
			
			protected override void OnDisposeManaged()
			{
			}
			
			protected override void OnDisposeUnmanaged()
			{
			}
		}
	}
}

#endif
