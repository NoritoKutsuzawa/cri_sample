﻿/****************************************************************************
 *
 * Copyright (c) 2014 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

#if !UNITY_EDITOR && (UNITY_PSP2 || UNITY_PS4 || UNITY_XBOXONE || UNITY_SWITCH)
	#define CRIWARE_SAMPLELIST_UNSUPPORT_NETWORK_ACCESS
#endif

#if (UNITY_EDITOR && !UNITY_EDITOR_LINUX) || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_ANDROID || UNITY_IOS
	#define CRIWARE_SAMPLELIST_SUPPORT_VP9
#endif

using System.Collections.Generic;

public static class Scene_00_SampleListData {

	public static bool supportNetworkAccess { get {
	#if !CRIWARE_SAMPLELIST_UNSUPPORT_NETWORK_ACCESS
			return true;
	#else
			return false;
	#endif
		} }

	public const string Title = "< CRI Samples >";
	public static readonly Dictionary<string, string[,]> SceneDict = new Dictionary<string, string[,]>(){
		{"Filesystem_Basic",
			new string[,] {
#if !UNITY_WEBGL
				{"Scene_01_LoadFile",
					"This sample loads files in\nthe StreamingAssets folder\nor files on the network."
				},
				{"Scene_02_BindCpk",
					"This sample binds a CPK file\nand loads the content file from the CPK file."
				},
#if !CRIWARE_SAMPLELIST_UNSUPPORT_NETWORK_ACCESS
				{"Scene_03_Install",
					"This sample installs files on the network.\nInstalled files are stored on the local storage,\nand loaded from the storage."
				},
				{"Scene_04_MultiBind",
					"This sample binds multiple CPK files\nat the same time."
				},
#endif
				{"Scene_05_BindDirectory",
					"This sample binds a directory\ninstead of using a CPK file."
				},
				{"Scene_06_NativeApi",
					"This sample uses the native API of\nCRI File System directly.\nThis sample is for CRI File System users."
				},
#endif
#if !CRIWARE_SAMPLELIST_UNSUPPORT_NETWORK_ACCESS
				{"Scene_07_WebInstall",
					"This sample installs file on the network\nby using CriFsWebInstaller."
				}
#endif
			}
		},
#if !CRIWARE_SAMPLELIST_UNSUPPORT_NETWORK_ACCESS
		{"Filesystem_Advanced",
			new string[,] {
				{"Scene_01_BatchWebInstall",
					"This sample installs files on the network\nby using BatchWebInstaller sample script."
				}
			}
		},
#endif
		{"Atom_Basic",
			new string[,] {
				{"Scene_01_SimplePlayback",
					"A simple sample that plays a sound\nby clicking the cube."
				},
				{"Scene_02_3DPosition",
					"This sample demonstrates distance attenuation\nby 3D positioning for the helicopter."
				},
				{"Scene_03_AISAC",
					"This sample demonstrates parameter control\nby AISAC.\nMoving the slider changes the pitch."
				},
				{"Scene_04_ControlParameter",
					"This sample demonstrates parameter control\nby SoundSource.\nMoving the slider changes the tone."
				},
				{"Scene_05_Category",
					"This sample demonstrates parameter control\nby Category.\nMoving the slider changes the volume."
				},
				{"Scene_06_BlockPlayback",
					"This sample demonstrates block playback\nBy changing the block during playback,\nsound status changes."
				},
#if !UNITY_WEBGL
				{"Scene_07_Ingame_Pinball",
					"A pinball game-like sample.\nWhile sounds are played by script,\nthe In-game preview connection\nfrom CRI Atom Craft is possible."
				},
#endif
				{"Scene_08_PlaybackWithDecryption",
					"This sample demonstrates decryption\nand playback of\nthe encrypted sound data."
				},
			}
		},
		{"Atom_Advanced",
			new string[,] {
#if !UNITY_WEBGL
				{"Scene_01_BackgroundLoadData",
					"This sample plays BGM\nwhile loading image in the background."
				},
#endif
				{"Scene_02_OverScenePlayback",
					"This sample switches scenes\nwithout interrupting the BGM.\nYou can switch scenes\nby clicking on the cube."
				},
#if UNITY_IOS
				{"Scene_03_PlaybackWithOtherAudio",
					"This sample mutes the BGM category\nwhen another application is playing sounds.\nThis sample assumes the system is iOS."
				},
#endif
#if !CRIWARE_SAMPLELIST_UNSUPPORT_NETWORK_ACCESS && !UNITY_WEBGL
				{"Scene_04_PlaybackWithInstall",
					"This sample installs sound data via the network\nand plays the installed data."
				},
#endif
#if UNITY_ANDROID
				{"Scene_05_EstimateSoundLatency",
					"This sample gets the estimated value of\nsound playback latency on Android\nThis sample assumes the system is Android."
				},
#endif
			}
		},
#if !UNITY_WEBGL
		{"Atom_Primitive",
			new string[,] {
				{"Scene_01_PlaybackFile",
					"A simple sample that plays a sound file."
				},
			}
		},
#endif
		{"Atom_Script",
			new string[,] {
				{"ScriptSample01_ClickToPlay",
					"This sample plays a sound\nwhen an object is clicked."
				},
				{"ScriptSample02_SoundTest",
					"This sample displays a button\nfor each Cue in the CueSheet file(ACB file)."
				},
				{"ScriptSample03_TitleScene",
					"This sample cross-fades music\nwhen scenes are switched."
				},
				{"ScriptSample04_TitleScene",
					"This sample can start from any scene\nwhile controlling sounds across scenes."
				},
				{"ScriptSample05_LevelMeter",
					"This sample gets the volume in real time\nand visualizes it by the cube size."
				},
				{"ScriptSample06_OutputCapture",
					"This sample captures pcm output from player\nand visualizes sound wave."
				},
				{"GameSample_Pinball",
					"Pinball game sample project\nfor CRI Atom Craft"
				},
			}
		},
#if UNITY_IOS || UNITY_ANDROID || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_PS4 || UNITY_XBOXONE || UNITY_SWITCH
		{"Atom_Expansion",
			new string[,] {
				{"Scene_01_McDSP_Preview",
					"This sample demonstrates how McDSP cooperates\nwith CRI Atom."
				}
			}
		},
#endif
#if !UNITY_PS3
		{"Mana_Basic",
			new string[,] {
				{"Scene_01_SimpleMoviePlayback",
					"A simple movie playback.\nThe movie is attached to texture on the cube."
				},
				{"Scene_02_AlphaMovie",
					"This sample plays an alpha movie.\nAlpha movie for STAGE START is played."
				},
				{"Scene_03_PlaybackCpk",
					"This sample plays a movie from a CPK file\nBinding by CRI File System is used to play."
				},
#if !CRIWARE_SAMPLELIST_UNSUPPORT_NETWORK_ACCESS && !UNITY_WEBGL
				{"Scene_04_DownloadPlayback",
					"This sample plays a downloaded movie.\nAfter a CPK file is downloaded\nand saved to local storage,\nthe movie is played."
				},
#endif
				{"Scene_05_Seek",
					"This sample performs seek playback.\nThe playback is done by script."
				},
				{"Scene_06_SeamlessSequencePlayback",
					"This sample performs\nseamless concatenated playback.\nThe playback is done by script."
				},
#if !UNITY_WEBGL
				{"Scene_07_H264Playback",
					"Simple playback of H.264 movie.\nThe movie is attached to a texture on the cube."
				},
#endif
			}
		},
		{"Mana_Advanced",
			new string[,] {
				{"Scene_01_AlphaAndAdditive",
					"This sample plays\nalpha movie and additive blending movie."
				},
				{"Scene_02_DynamicMovieSwitch",
					"This sample switches movies dynamically.\nSwitching during playback is performed\nseamlessly to play the next movie."
				},
				{"Scene_03_JigsawMovie",
					"This sample cuts part of a movie.\nBy adjusting the uv coordinates of mesh,\nthe movie is cut and displayed."
				},
			}
		},
#if CRIWARE_SAMPLELIST_SUPPORT_VP9
		{"Mana_Expansion",
			new string[,] {
				{"Scene_01_Vp9Playback",
					"Simple VP9 playback sample"
				},
			}
		},
#endif
#endif
	};
}
