﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、CRI Atom と CRI File System のユーティリティAPIを使い、
 * BGMをストリーミング再生しながら画像ファイルの裏読みを行います。
 * 画像データはローカルとURLで指定したリモートホスト上の両方から読み込みますが、
 * 画像ファイルのロード中であってもBGMのストリーミングが途切れないことを確認できます。
 * 原則、アプリが CRI File System を使ってファイルのロードを行う限り、
 * CRI Atom や CRI Mana のストリーミング再生が途切れる事はありません。
 */
/*EN
 * This sample loads an image file in the background while performing the streaming
 * playback of the BGM by using CRI Atom and CRI File System's utility APIs.
 * The image data both on the local storage and on the remote host specified by URL
 * can be loaded, and even if the data is being loaded, the BGM streaming will
 * not be interrupted.
 * In principle, the streaming playback of CRI Atom and CRI Mana will not
 * be interrupted as long as the application performs the file loading by 
 * CRI File system.
 */

using UnityEngine;
using System.Collections;
using System.IO;

public class Scene_01_BackgroundLoadData : MonoBehaviour 
{
	#region Variables
	/* the number of background loading of image file at one time */
	private const int maxImages = 3;
	/* path on the remote host(URL) */
	private const string remoteUrl = "http://crimot.com/sdk/sampledata/crifilesystem/";

	/* parameters to store the loaded data */
	private Texture2D[] textures = new Texture2D[maxImages];
	/* lock counter for GUI button */
	private int lockLoadImageFile = 0;

	/* the Game object to which the AtomSource for BGM playback is attached (which is set in Unity editor) */
	public GameObject bgmPlayer;
	#endregion

	#region Functions
	/* Load an image file. */
	private IEnumerator LoadImageFile(string path, int request_index)
	{
		lockLoadImageFile++;
		/* Request to load a file. */
		var request = CriFsUtility.LoadFile(path);
		/* Wait until the loading is complete. */
		yield return request.WaitForDone(this);
		lockLoadImageFile--;

		if (request.error == null) {
			/* If successful, create a texture from the byte string. */
			this.textures[request_index] = new Texture2D(0, 0);
			this.textures[request_index].LoadImage(request.bytes);
		} else {
			Debug.LogWarning("Failed to load image file.");
		}
	}

	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			/* Disable GUI operations while displaying a list of samples. */
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");
		
		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		/* Display the loaded image. */
		if (this.textures[0]) {
			GUI.DrawTexture(new Rect(Screen.width / 2 - this.textures[0].width / 2, Screen.height / 2 - this.textures[0].height / 2 + 32,
				this.textures[0].width, this.textures[0].height), this.textures[0]);
		}
		for (int i = 0; i < 2; i++) {
			if (this.textures[i + 1]) {
				GUI.DrawTexture(new Rect(300 * i, 120, this.textures[i + 1].width, this.textures[i + 1].height), this.textures[i + 1]);
			}
		}

		/* Get the Atom Source component from the property. */
		CriAtomSource bgm_player = bgmPlayer.GetComponent<CriAtomSource>();
		/* Get the playback status of the Atom Source. */
		CriAtomSource.Status status = bgm_player.status;
		
		GUILayout.BeginArea(new Rect(Screen.width - 396, 70, 380, 400));
		
		if ((status == CriAtomSource.Status.Prep) || (status == CriAtomSource.Status.Playing)) {
			/* Check the status, and display the stop button if the playback status is in the preparation or in the playing. */
			if (Scene_00_GUI.Button("Stop Streaming BGM")) {
				/* Stop the BGM playback. */
				bgm_player.Stop();
			}
		} else {
			/* Check the status, and display the start button if the playback status is other than in the preparation and in the playing. */
			if (Scene_00_GUI.Button("Playback Streaming BGM")) {
				/* Start the BGM playback. */
				/* Play with the Cue name that is set in Unity editor. */
				/* This sample sets a Cue name for streaming playback. */
				bgm_player.Play();
			}
		}
		GUILayout.Space(16);
		
		/* a flag to store whether the GUI button for loading an image file was clicked. */
		bool requestLoadImage = false;
		/* file path to load an image file (set the content depending on the GUI button pressed) */
		string dir = "";

		if (lockLoadImageFile == 0) {
			/* Depending on the GUI button clicked, switch the source of the image file between local and remote. */
			if (Scene_00_GUI.Button("Load Image File (Local)")) {
				/* Set the path to the local image file. */
				dir = CriWare.streamingAssetsPath;
				/* Remember that the GUI button for loading an image file was clicked. */
				requestLoadImage = true;
			}
			if (Scene_00_SampleListData.supportNetworkAccess) {
				if (Scene_00_GUI.Button("Load Image File (URL)")) {
					/* Set the path to the remote image file. */
					dir = remoteUrl;
					/* Remember that the GUI button for loading an image file was clicked. */
					requestLoadImage = true;
				}
			}
		} else {
			/* Disable the GUI buttons if the image file is already being loaded. */
			Scene_00_GUI.Button("Loading ...");
			if (Scene_00_SampleListData.supportNetworkAccess) {
				Scene_00_GUI.Button("Loading ...");
			}
		}
		
		GUILayout.Space(16);
		if (Scene_00_GUI.Button("Delete Images")) {
			/* Delete the textures for displaying the loaded image file. */
			for (int i = 0; i < maxImages; i++) {
				this.textures[i] = null;
			}
		}
		
		GUILayout.EndArea();
		
		if (requestLoadImage) {
			/* If a request to load an image file is received, load the file. */
			/* This sample loads multiple times to put more stress on the I/O. */
			int i = 0;
			StartCoroutine(this.LoadImageFile(Path.Combine(dir, "criware.png"), i++));
			StartCoroutine(this.LoadImageFile(Path.Combine(dir, "sample_image1.png"), i++));
			StartCoroutine(this.LoadImageFile(Path.Combine(dir, "sample_image2.png"), i++));
		}
		
		Scene_00_GUI.EndGui();


	}
	#endregion
}
/* end of file */
