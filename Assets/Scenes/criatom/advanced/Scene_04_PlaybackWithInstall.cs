﻿/****************************************************************************
 *
 * Copyright (c) 2014 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、ネットワーク経由でADX2のデータ(ACF, ACB)をインストール、再生を行うサンプルです。
 */
/*EN
 * This sample installs ADX2 data(ACF, ACB) via the network and plays the installed data.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Scene_04_PlaybackWithInstall : MonoBehaviour {
	#region PublicVariables
	public CriAtomSource atomSource;
	#endregion
	
	#region PrivateVariables
	/* URL prefix for the installation source */
	private const string RemoteUrl = "http://crimot.com/sdk/sampledata/criatom/advanced/Common/";

	/* ACF, ACB file names */
	private const string AcfFilename = "dlc_Pinball.acf";
	private const string AcbFilename = "dlc_PinballMain.acb";
	/* CueSheet name */
	private const string cueSheetName = "PinballMain";
	
	/* the installation directory */
	private string localDataDir;
	/* the installation path */
	private string acfFilePath;
	private string acbFilePath;
	/* request for installation */
	private CriFsInstallRequest installRequest = null;
	/* file name for installation */
	private string installingFilename;
	/* ACF, ACB are registered? */
	private bool   registerdToAtom = false;
	#endregion

	void Start()
	{
		/* Make the installation path. */
		localDataDir = CriWare.installTargetPath;
		acfFilePath  = Path.Combine(localDataDir, AcfFilename);
		acbFilePath  = Path.Combine(localDataDir, AcbFilename);
	}
	
	void OnDisable()
	{
		StopInstall();
	}
	
	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;

		/* GUI for this sample scene. */
		GUILayout.BeginArea(new Rect(60, 80 , 400, 400));
		{
			bool dataIsInstalled = DataIsInstalled();
			/* GUI for the installation control */
			GUILayout.Label("Install Control");
			{
				if (InstallerIsRrunning()) {
					GUILayout.Label("Installing: " + installingFilename);
					GUILayout.Label("   " + (installRequest.progress * 100) + "%");
					if (Scene_00_GUI.Button("Stop")) {
						StopInstall();
					}
				} else {
					GUI.enabled = !dataIsInstalled && !registerdToAtom;
					/* 1. Installs ACF and ACB via the network. */
					if (Scene_00_GUI.Button("1. Install")) {
						StartCoroutine(
							SequenceInstall(
							new string[,]{
								{RemoteUrl + AcfFilename, acfFilePath},
								{RemoteUrl + AcbFilename, acbFilePath}
							}));
					}
					GUI.enabled = dataIsInstalled && !registerdToAtom;
					if (Scene_00_GUI.Button("Delete installed data")) {
						if (File.Exists(acfFilePath)) {
							File.Delete(acfFilePath);
						}
						if (File.Exists(acbFilePath)) {
							File.Delete(acbFilePath);
						}
					}
				}
			}
			/* GUI for the Atom control */
			GUILayout.Label("Atom Control");
			{
				GUI.enabled = dataIsInstalled && !registerdToAtom;
				/* 2. Registers the installed data to Atom. */
				if (Scene_00_GUI.Button("2. Register ACF, ACB")) {
					RegisterAcfAcb();
				}
				GUI.enabled = dataIsInstalled && registerdToAtom;
				if (Scene_00_GUI.Button("Unregister ACF, ACB")) {
					UnregisterAcfAcb();
				}
				if (Scene_00_GUI.Button("3. Play cue 'glass'")) {
					/* 3. Plays the installed data. */
					atomSource.cueSheet = cueSheetName;
					atomSource.Play("glass");
					Debug.Log("Play cue 'glass'");
				}
			}
			GUI.enabled = true;
		}
		GUILayout.EndArea();
		
		Scene_00_GUI.EndGui();
	}
	
	#region InstallControl
	private bool DataIsInstalled()
	{
		return File.Exists(acfFilePath) && File.Exists(acbFilePath);
	}
	
	private bool InstallerIsRrunning()
	{
		return installRequest != null;
	}
	
	/* Coroutine for installing files */
	private IEnumerator Install(string srcPath, string dstPath)
	{
		if (InstallerIsRrunning()) {
			Debug.LogError("InstallerIsRrunning");
			yield break;
		}
		/* A request for installation */
		installRequest  = CriFsUtility.Install(null, srcPath, dstPath);
		installingFilename = Path.GetFileName(dstPath);
		/* Wait untill the installation is completed. */
		yield return installRequest.WaitForDone(this);
		if (installRequest.error == null) {
			Debug.Log("Completed installation." + srcPath);
		} else {
			Debug.LogWarning("Failed installation." + srcPath);
		}
		/* Release the request after completing the installation. */
		installingFilename = null;
		installRequest.Dispose();
		installRequest = null;
	}
	
	/* Coroutine for installing multiple files continuously */
	private IEnumerator SequenceInstall(string[,] srcDstPairs)
	{
		if (InstallerIsRrunning()) {
			Debug.LogError("InstallerIsRrunning");
			yield break;
		}
		for (int i = 0; i < srcDstPairs.GetLength(0); i++) {
			yield return StartCoroutine(Install(srcDstPairs[i, 0], srcDstPairs[i, 1]));
		}
	}
	
	/* Stop the installation. */
	private void StopInstall()
	{
		if (InstallerIsRrunning()) {
			installRequest.Dispose();
			installRequest = null;
		}
	}
	#endregion
	
	
	#region AtomControll
	/* Register the installed data to Atom. */
	private void RegisterAcfAcb()
	{
		/* Register the installed ACF. */
		CriAtomEx.RegisterAcf(null, acfFilePath);
		/* Add the installed ACB. */
		CriAtom.AddCueSheet(cueSheetName, acbFilePath, null);
		Debug.Log("RegisterAcfAcb. " + Path.GetFileName(acfFilePath) + ", " + Path.GetFileName(acbFilePath));
		registerdToAtom = true;
	}
	
	/* Relase ACF and ACB. */
	private void UnregisterAcfAcb()
	{
		/* Release ACB. */
		CriAtom.RemoveCueSheet(cueSheetName);
		/* Release ACF. */
		CriAtomEx.UnregisterAcf();
		registerdToAtom = false;
	}
	#endregion
}
