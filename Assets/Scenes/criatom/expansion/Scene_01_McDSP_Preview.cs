﻿/****************************************************************************
 *
 * Copyright (c) 2017 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、McDSPエフェクトの切り替え再生を行います。
 * 各エフェクトのボタンをクリックするとエフェクトの効果を確認ができます。
 *
 * CubeオブジェクトにMcDSPエフェクト付きAtomSourceが設定されており、
 * ボタンを押すと各種類エフェクトが使用するバスの音量調整が行われ、
 * 単独エフェクトがかかった状態の音声が再生されます。
 * McDSPプラグインの初期化はCriMcDspiInitializerで行われます。（スクリプトの実行順序をご注意ください）
 */
/*EN
 * This sample shows how to switch among McDSP effects.
 * Please check the result sound by clicking the corresponding effect button.
 *
 * A ACB file(with McDSP Effects applied) is assigned to the Cube object by the CRI Atom Source component.
 * By clicking any effect buttons, the bus level corresponding to the effect will be changed,
 * hence only one effect will be audible.
 * The McDSP Plugin for Unity is initialized by CriMcDspiInitializer.cs.
 * (Please be aware about the script execution order.)
 */
using UnityEngine;
using System.Collections;

public class Scene_01_McDSP_Preview : MonoBehaviour
{
	#region Variables
	public CriAtomSource atomSource;

	private struct McDspEffectBusInfo {
		public readonly string busName;     /* バス名					*/
		public readonly string effectName;  /* エフェクト名				*/
		public readonly uint numParams;     /* エフェクトのパラメータ数	*/
		public readonly int paramId;		/* パラメータ書き込み先id		*/
		public McDspEffectBusInfo(string busName, string effectName, uint numParams, int paramId)
		{
			this.busName = busName;
			this.effectName = effectName;
			this.numParams = numParams;
			this.paramId = paramId;
		}
	}
	private McDspEffectBusInfo[] effectList = new McDspEffectBusInfo[] {
		new McDspEffectBusInfo("MasterOut",	"MasterOut",					0,		-1),
		new McDspEffectBusInfo("BUS1",		"CRIWARE/Distortion",			4,		-1),
		new McDspEffectBusInfo("BUS2",		"McDSP/AE600 Active EQ",		131,	0),
		new McDspEffectBusInfo("BUS3",		"McDSP/FutzBox",				40,		1),
		new McDspEffectBusInfo("BUS4",		"McDSP/ML4000 ML1",				5,		2),
		new McDspEffectBusInfo("BUS5",		"McDSP/SA2 Dialog Processor",	14,		3)
	};
	private readonly string[] snapshotList = new string[] {
		"Snapshot1",
		"Snapshot2"
	};
	private const string dspBusName = "DspBusSetting_0";
	private int currentBusId = 0;
	private int currentSnapshot = -1;
	#endregion

	#region Functions
	private void Start()
	{
		CriAtomEx.AttachDspBusSetting(dspBusName);
		SwitchBus("MasterOut", 1.0f);
	}

	void OnGUI()
	{
#if UNITY_WEBGL
		if (CriAtom.CueSheetsAreLoading) {
			return;
		}
#endif
		
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;

		if (Scene_00_GUI.Button(new Rect(50, 100, 160, 100), "Play Cue 1")) {
			atomSource.Play(1);
		}
		if (Scene_00_GUI.Button(new Rect(50, 210, 160, 100), "Play Cue 2")) {
			atomSource.Play(2);
		}
		if (Scene_00_GUI.Button(new Rect(50, 320, 160, 100), "Stop All")) {
			if (atomSource.status == CriAtomSource.Status.Playing) {
				atomSource.Stop();
				SwitchBus("MasterOut", 1.0f);
			}
		}
		if (Scene_00_GUI.Button(new Rect(50, 430, 160, 100), "Reset Effect")) {
			CriAtomEx.DetachDspBusSetting();
			currentSnapshot = -1;
			CriAtomEx.AttachDspBusSetting(dspBusName);
			SwitchBus("MasterOut", 1.0f);
		}

		var buttonRect = new Rect(220, 100, 160, 100);
		for (int i = 0; i < snapshotList.Length; ++i) {
			if (Scene_00_GUI.Button(buttonRect, snapshotList[i])) {
				CriAtomEx.ApplyDspBusSnapshot(snapshotList[i], 3000);
				currentSnapshot = i;
			}
			buttonRect.y += 110;
		}
		
		buttonRect = new Rect(Screen.width - 450, 100, 400, 50);
		for (int i=0; i< effectList.Length; ++i) {
			if (Scene_00_GUI.Button(buttonRect, effectList[i].effectName)) {
				SwitchBus(effectList[i].busName, 1.0f);
			}
			buttonRect.y += 60;
		}
		buttonRect.y += 10;
		GUILayout.BeginArea(buttonRect, effectList[currentBusId].effectName, Scene_00_SampleList.TextStyle);
		GUILayout.EndArea();
		if(currentSnapshot >= 0) {
			buttonRect.y += 60;
			GUILayout.BeginArea(buttonRect, snapshotList[currentSnapshot]+" Applied", Scene_00_SampleList.TextStyle);
			GUILayout.EndArea();
		}

		Scene_00_GUI.EndGui();
	}

	private void SwitchBus(string busName, float level)
	{
		for(int i = 0; i < effectList.Length; ++i) {
			if(effectList[i].busName == busName) {
				currentBusId = i;
				atomSource.SetBusSendLevel(effectList[i].busName, level);
			} else {
				atomSource.SetBusSendLevel(effectList[i].busName, 0.0f);
			}
		}
	}
	#endregion
}
/* end of file */
