﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 *   本サンプルは、サウンドファイルの再生するシンプルなものです。
 *   
 * 　コンポーネントを使用せず、下記の関数コールによって簡単にサウンドファイル
 *   を再生できます。
 * 　
 *   (1) プレーヤーの作成 (new CriAtomExPlayer)
 *   (2) 再生するファイルの設定 (SetFile関数）
 *   (3) サウンドフォーマットの設定 (SetFormat関数)
 *   (4) 再生開始指示 (Start関数）
 * 
 *   圧縮されたサウンドファイルは、以下のようにコマンドラインツールによって作
 *   成(エンコード)できます。
 * 
 *   > criatomencd <入力ファイル名> <出力ファイル名> -codec=<圧縮コーデック名>
 * 
 *   [使用例]
 * 　sample_music.wavファイルをHCAコーデックでエンコードする。
 *   > criatomencd sample_music.wav sample_music.hca -codec=HCA
 *
 *   [注意]
 * 　　本サンプルは、現時点(2012-07-12)ではベータ版扱いとなります。今後、一部
 *　　 のAPI仕様が変更になる可能性がありますのでご注意ください。
 * 　　CriAtomExPlayerなどのドキュメントは、今後のリリースで公開予定です。
 * 　　Android,iOS版についても、今後、リリース予定です。
 * 
 */
/*EN
 *   This sample is simple one that plays a sound file.
 *   
 *   A sound file can easily be played by calling the following functions 
 *   without using componets.
 *   
 *   (1) Create a player. (new CriAtomExPlayer)
 *   (2) Set a file to play. (SetFile function）
 *   (3) Set sound format. (SetFormat function)
 *   (4) Start the playback. (Start function)
 * 
 *   A compressed sound file can be created (encoded) with the command line tool
 *   as shown below:
 * 
 *   > criatomencd <input file name> <output file name> -codec=<codec name>
 * 
 *   [Example]
 *  Encode sample_music.wav by the HCA codec.
 *   > criatomencd sample_music.wav sample_music.hca -codec=HCA
 *
 *   [Note]
 *   This sample is provided as the beta version as of 2012-07-12.
 *   Please note that API specifications may be changed in the future.
 *   Documents about the CriAtomExPlayer etc. will be released in the future.
 *   The Android and iOS editions will also be released in the future.
 * 
 */
using UnityEngine;
using System.Collections;
using System.IO;

public class Scene_01_PlaybackFile : MonoBehaviour
{
    #region Variables
	/* Atom player */
	private CriAtomExPlayer player;
	#endregion

    #region Functions
	/* Initialization process */
	void Start()
	{
		/* Create an Atom player. */
		/* When the number of simultaneous file playbacks exceeds the second argument of the constructor, an error occurs. */
		this.player = new CriAtomExPlayer(256, 8);
	}

	/* Play the sound file. */
	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;

		GUILayout.BeginArea(new Rect(Screen.width - 436, 65, 420, 400));

		/* Play the music (automatically repeated). */
		if (Scene_00_GUI.Button("Playback File (MUSIC)")) {
			/* Specify a file to be played. */
			this.player.SetFile(null, Path.Combine(CriWare.streamingAssetsPath, "sample_music.hca"));
			/* Specify the compression format to be played. */
			this.player.SetFormat(CriAtomEx.Format.HCA);
			/* Start playback. */
			this.player.Start();
		}
		/* Play the dialog. */
		if (Scene_00_GUI.Button("Playback File (VOICE)")) {
			this.player.SetFile(null, Path.Combine(CriWare.streamingAssetsPath, "sample_voice.hca"));
			this.player.SetFormat(CriAtomEx.Format.HCA);
			this.player.Start();
		}
		/* Play the sound effect. */
		if (Scene_00_GUI.Button("Playback File (SE)")) {
			this.player.SetFile(null, Path.Combine(CriWare.streamingAssetsPath, "sample_se_gun_shot.hca"));
			this.player.SetFormat(CriAtomEx.Format.HCA);
			this.player.Start();
		}

		GUILayout.Space(8);

		/* Stop the sound. */
		if (Scene_00_GUI.Button("Stop")) {
			/* Stop the sound that is played by the player. */
			this.player.Stop();
		}

		GUILayout.EndArea();
		
		Scene_00_GUI.EndGui();
	}
	/* Finalization process */
	void OnDisable()
	{
		/* Destroy the player. */
		player.Dispose();
	}
	#endregion
}

/* end of file */
