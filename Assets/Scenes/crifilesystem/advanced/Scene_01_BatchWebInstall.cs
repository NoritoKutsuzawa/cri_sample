﻿/****************************************************************************
 *
 * Copyright (c) 2016 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;
using System.Collections;

public class Scene_01_BatchWebInstall : MonoBehaviour
{
	BatchWebInstaller batchWebInstaller;

	const int installConcurrency = 2;

	static string DestPath(string relpath)
	{
		return System.IO.Path.Combine(CriWare.installTargetPath, relpath);
	}

	void Start ()
	{
		// Initialize CriFsWebInstaller module
		var config = CriFsWebInstaller.defaultModuleConfig;
		config.numInstallers = installConcurrency;
		CriFsWebInstaller.InitializeModule(config);

		// Create BatchWebInstaller
		batchWebInstaller = new BatchWebInstaller(installConcurrency);
		InitializeInstallQueue();
	}

	void OnDestroy()
	{
		// Destroy BatchWebInstaller
		batchWebInstaller.Dispose();
		batchWebInstaller = null;

		// Finalize CriFsWebInstaller module
		CriFsWebInstaller.FinalizeModule();
	}

	void Update ()
	{
		// CriFsWebInstaller.ExecuteMain is executed in the BatchWebInstaller.Update
		batchWebInstaller.Update();
	}

	private void InitializeInstallQueue()
	{
		// Add to install queue
		BatchWebInstaller.UrlAndDestPath[] installList = {
			new BatchWebInstaller.UrlAndDestPath("https://sdk.criware.com/support_e/sample/fs_sample_data/data00.dat", DestPath("data00.dat")),
			new BatchWebInstaller.UrlAndDestPath("https://sdk.criware.com/support_e/sample/fs_sample_data/data01.dat", DestPath("data01.dat")),
			new BatchWebInstaller.UrlAndDestPath("https://sdk.criware.com/support_e/sample/fs_sample_data/data02.dat", DestPath("data02.dat")),
			new BatchWebInstaller.UrlAndDestPath("https://sdk.criware.com/support_e/sample/fs_sample_data/data03.dat", DestPath("data03.dat")),
			new BatchWebInstaller.UrlAndDestPath("https://sdk.criware.com/support_e/sample/fs_sample_data/data04.dat", DestPath("data04.dat")),
			new BatchWebInstaller.UrlAndDestPath("https://sdk.criware.com/support_e/sample/fs_sample_data/data05.dat", DestPath("data05.dat")),
			new BatchWebInstaller.UrlAndDestPath("https://sdk.criware.com/support_e/sample/fs_sample_data/data06.dat", DestPath("data06.dat")),
			new BatchWebInstaller.UrlAndDestPath("https://sdk.criware.com/support_e/sample/fs_sample_data/data07.dat", DestPath("data07.dat")),
			new BatchWebInstaller.UrlAndDestPath("https://sdk.criware.com/support_e/sample/fs_sample_data/data08.dat", DestPath("data08.dat")),
		};
		batchWebInstaller.installQueue.Clear();
		batchWebInstaller.installQueue.AddRange(installList);

		foreach (var item in batchWebInstaller.installQueue) {
			System.IO.File.Delete(item.destPath);
		}
	}

	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}

		Scene_00_GUI.BeginGui("01/SampleMain");

		// Set UI skin
		GUI.skin = Scene_00_SampleList.uiSkin;

		GUILayout.BeginArea(new Rect(40, 100 , 800, 400));

		// Display Install Status
		string statusString =
			"Status: " + batchWebInstaller.status.ToString() + "\n" +
			"Queue Count: " + batchWebInstaller.installQueue.Count.ToString() + "\n";
		for (int i = 0; i < batchWebInstaller.installContexts.Length; i++) {
			var installContext = batchWebInstaller.installContexts[i];
			var statusInfo = installContext.statusInfo;
			statusString += "\n" +
				((installContext.urlAndDestPath != null) ? System.IO.Path.GetFileName(installContext.urlAndDestPath.destPath) : "----------") + ": " +
				statusInfo.status.ToString() +
				": " + statusInfo.error.ToString() +
				": " + statusInfo.httpStatusCode.ToString() +
				": " + (float)statusInfo.receivedSize / (statusInfo.contentsSize == -1 ? 1 : statusInfo.contentsSize) * 100 + " %";
		}
		GUILayout.Label(statusString);

		GUILayout.Space(8);
		GUI.enabled = (batchWebInstaller.status == BatchWebInstaller.Status.Stop);
		if (Scene_00_GUI.Button("Initialize Install Queue", GUILayout.Width(400))) {
			InitializeInstallQueue();
		}
		if (Scene_00_GUI.Button("Start or Resume Batch Install", GUILayout.Width(400))) {
			// Start or Resume batch web install
			batchWebInstaller.Start();
		}
		GUI.enabled = (batchWebInstaller.status != BatchWebInstaller.Status.Stop);
		if (Scene_00_GUI.Button("Stop", GUILayout.Width(400))) {
			// Stop batch web install
			batchWebInstaller.Stop();
		}
		GUI.enabled = true;

		GUILayout.EndArea();

		Scene_00_GUI.EndGui();
	}
}
