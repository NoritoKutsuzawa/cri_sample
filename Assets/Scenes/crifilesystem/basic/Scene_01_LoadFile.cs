﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは。ローカルドライブとネットワークにあるファイルを読み込む
 * サンプルです。サンプルプログラムは、以下の3つソースファイルによって
 * 構成されます。
 * 
 * （１）読み込みスタイル切り替え部          :Scene_01_LoadFile.cs
 * （２）従来スタイルの読み込みサンプル      :Scene_01_LoadFile_Ordinary.cs
 * （３）コルーチンスタイルの読み込みサンプル:Scene_01_Loadfile_Coroutine.cs
 *
 * Unity向けCRI File Systemライブラリでは、以下の2つの読み込み方法を
 * サポートします。
 * 
 *  (1) 従来スタイル (ORDINALY_STYLE)
 *    各フレームの更新時に、ポーリングによって読み込み終了を確認する
 * 　 従来のコンシューマゲーム開発で使われている読み込み方法です。
 *  (2) コルーチンスタイル (COROUTINE_STYLE)
 *    コルーチンによって、読み込み終了をyeild命令によって待ちます。
 *    読み込み処理をシンプルに記述することができます。
 *   
 *  「従来スタイル」では、下記の手順によって読み込んでいます。
 *   (1) ファイルサイズの取得
 * 　(2) 読み込みバッファの確保
 * 　(3) 読み込みリクエストの発行
 * 　(4) 読み込み完了のチェック
 *   (5) 読み込まれたデータの処理
 * 
 * 　ファイルを取り扱う際に、バインダとファイル名によってファイルを指定
 * 　します。
 * 　バインダは、仮想的なドライブやデバイスのようなものです。
 * 　バインダは重要なオブジェクトで、これによって柔軟な読み込みを実現し
 *   ています。
 * 　バインダにパックしたファイルやディレクトリを結びつけることによって、
 * 　読み込み元を簡単に切りかえることができます。
 *   本サンプルでは、バインダに対して何も設定してませんので、ローカル
 *   ファイルやネットワークにある単体ファイルを対象としています。
 * 　バックされたファイルから読み込む場合は、Scene_07_NativeAPIサンプル
 * 　をご参照ください。
 * 
 *   「コルーチンスタイル」では、下記の手順によって読み込んでいます。
 *   (1) コルーチンを生成しスタート。以降はコルーチン内での処理。
 *   (2) 読み込みリクエストの発行
 * 　(3) yeild 命令により、読み込み完了を待つ。
 *   (4) 読み込まれたバッファをリクエストオブジェクトから取得
 * 　(5) 読み込まれたデータの処理
 * 
 * 　コルーチン内で読み込みリクエストを発行すると、リクエストオブジェク
 *   トが返ります。
 *   yeild命令で読み込み完了を待ちます。ファイルシステムは、バックグラ
 *   ウンドで自動的にバッファを確保し、読み込みを実行します。
 *   Update関数の間隔で状態は更新され、読み込みが完了するとyeild命令の
 *   先のコードから処理が再開されます。
 * 
 *  [注意]　ファイルサイズの取得について　(従来スタイル)
 *   本サンプルでは、バインダに何も設定していないため、処理がブロックさ
 *   れます。これは、OSのファイルサイズ取得APIを使用したり、HTTPプロト
 *   コルで問い合わせているためです。
 * 　即時にファイルサイズを取得するためには、パックしたファイルを使用
 *   します。
 *     
 * 　CriFsBinder binder = new CriFsBinder();
 *    binder.BindCpk("sample.cpk");
 * 
 *   上記のように、バインダにパックファイルをバインドすると、ファイル
 *   サイズのテーブルをメモリ上に確保します。これによって、即時にファ
 *   イルサイズを取得できるようになります。
 *   これは、ネットワーク上のパックしたファイルでも同様にファイルサイ
 *   ズを即時に取得できます。
 * 　単体のファイルに対して、処理をブロックせずにファイルサイズを取得
 *   したい場合は、BindFile関数を使用してください。バインド完了を待っ
 *   てから取得することで処理がブロックされません。
 * 
 */
/*EN
 * This sample loads a file on the local storage or on the network.
 * It is composed of the following three source files.
 * 
 * (1) Module of switching the loading styles            : Scene_01_LoadFile.cs
 * (2) Sample of the file loading in the ordinary style  : Scene_01_LoadFile_Ordinary.cs
 * (3) Sample of the file loading in the coroutine style : Scene_01_Loadfile_Coroutine.cs
 *
 * The following two loading methods are available in the CRI File System for Unity.
 * 
 * (1) Ordinary style (ORDINALY_STYLE)
 *  This method checks whether the loading is complete by polling periodically
 *  when each frame is updated. It is used in developing traditional consumer games.
 *
 * (2) Coroutine style (COROUTINE_STYLE)
 *  This method uses a coroutine to wait for the loading to complete with the yield statement.
 *  It can simplify the loading process. 
 *
 *  In the ordinary style, a file is loaded with the following procedure:
 *   (a) Get the file size.
 *   (b) Allocate a read buffer.
 *   (c) Issue a request for file loading.
 *   (d) Check the completion of loading.
 *   (e) Process the loaded data.
 * 
 *  A binder and a file name is specified to handle a file.
 *  Binder is something like a virtual drive or device. 
 *  It is an important object to implement flexible file loading.
 *  By assigning a packed file or a directory to a binder, the source of 
 *  file loading can be switched easily.
 *  This sample does not configure the setting of binder, and it handles 
 *  a stand-alone file on the local storage or on the network.
 *  To load a file from a packed file, please see the Scene_06_NativeAPI sample.
 *
 *  In the coroutine, a file is loaded with the following procedure:
 *   (a) Create and start the coroutine. The rest of the process is done in the coroutine.
 *   (b) Issue a request for file loading.
 *   (c) Wait for the loading to complete with the yield statement.
 *   (d) Get the loaded data from the request object.
 *   (e) Process the loaded data.
 * 
 *   When a request for file loading is issued in the coroutine, a request object
 *   is returned. 
 *   The yield statement waits for the completion of file loading.
 *   The file system automatically allocates a buffer in the background, and 
 *   performs the file loading.
 *   The status is updated periodically by the Update function, and when 
 *   the file loading is completed, the process is resumed from the next step
 *   where the yield statement is defined.
 * 
 *  [Note] About the retrieval of the file size (in the ordinary style)
 *  This sample does not configure the setting of binder, therefore 
 *  the process will be blocked.  This is caused by using the file size 
 *  retrieval API or communicating with a server via the HTTP protocal.
 *  To instantly get the file size, please use a packed file.
 *     
 *   CriFsBinder binder = new CriFsBinder();
 *    binder.BindCpk("sample.cpk");
 * 
 *   As showon above, when binding a packed file to a binder, the table of
 *   file sizes is stored in memory. This enables to get the file size 
 *   immediately and to get the file size of a packed file on the network as well.
 *   To get the file size of a stand-alone file without blocking the process, 
 *   please use the BindFile function. The process is not blocked if waiting
 *   for the completion of binding.
 */

using UnityEngine;
using System;

/*	Switch the loading styles	*/
public class Scene_01_LoadFile : MonoBehaviour
{
    #region Variables
    /*	loading style	*/
    enum FrameworkStyle
    {
        ORDINARY_STYLE,				/*	ordinary style			*/
        COROUTINE_STYLE				/*	coroutine style			*/
    };
    /*	control variable for loading style	*/
    private FrameworkStyle style = FrameworkStyle.ORDINARY_STYLE;
	Component load_component;
    #endregion

    #region Functions
    /*	Create the loading component of ordinary style at the start-up.	*/
    void OnEnable()
    {
		load_component = this.gameObject.AddComponent<Scene_01_LoadFile_Ordinary>();
    }

    /*	Destroy the loading componet at the finalization.	*/
    void OnDisable()
	{
		Destroy(load_component);
	}
	
    /*	The GUI control function	 * */
    void OnGUI()
    {
        if (Scene_00_SampleList.ShowList == true)
        {
            return;
        }
		
		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		GUILayout.BeginArea(new Rect(Screen.width - 416, 70, 400, 800));
		
		string style_string;
		
		/*	Set the character string to be displayed on the button.	*/
        if ( style == FrameworkStyle.ORDINARY_STYLE) {
            style_string = "Loading Style: ORDINARY ";
		} else {
            style_string = "Loading Style: COROUTINE";
		}
        /*	Switch the style of loading component.	*/
		if (Scene_00_GUI.Button(style_string))
        {
            if (style == FrameworkStyle.ORDINARY_STYLE) {
                style = FrameworkStyle.COROUTINE_STYLE;
				Destroy(load_component);		/*	Destroy the component.	*/
				/*	Create the loading component of coroutine style.	*/
				load_component = this.gameObject.AddComponent<Scene_01_LoadFile_Coroutine>();
			} else {
                style = FrameworkStyle.ORDINARY_STYLE;
				Destroy(load_component);
			    /*	Create the loading component of ordinary style.	*/
				load_component = this.gameObject.AddComponent<Scene_01_LoadFile_Ordinary>();
			}
        }
		
		GUILayout.EndArea();
		
		Scene_00_GUI.EndGui();
    }
    #endregion
}


/* end of file */
