﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;

/*	コルーチンスタイル(COROUTINE_STYLE)による読み込み	*/
/*	Loading by the coroutine style (COROUTINE_STYLE)	*/
public class Scene_01_LoadFile_Coroutine : MonoBehaviour
{
    #region Variables
    /*	text data				*/
    private string loadedText;
    /*	texture					*/
    private Texture2D[] textures = new Texture2D[2];
    #endregion

    #region Functions
    private IEnumerator LoadTextFile(string path)
    {
        /*	Issue a request for file loading.				*/
        var request = CriFsUtility.LoadFile(path);
        /*	Sleep until the processing is completed	.		*/
        yield return request.WaitForDone(this);
        /*	Check whether the file loading is successful.	*/
        if (request.error == null)
        {
            /*	The request.bytes stores the loaded data.		*/
            using (var st = new StreamReader(new MemoryStream(request.bytes), Encoding.UTF8)) {
				this.loadedText = st.ReadToEnd();
			}
        }
        else
        {
            Debug.LogWarning("Failed to load text file. (path=" + path + ")");
        }
    }

    private IEnumerator LoadImageFile(string path, int n)
    {
        /*	Issue a request for file loading.				*/
        var request = CriFsUtility.LoadFile(path);
        /*	Sleep until the processing is completed	.		*/
        yield return request.WaitForDone(this);
        /*	Check whether the file loading is successful.	*/
        if (request.error == null)
        {
            this.textures[n] = new Texture2D(0, 0);
            /*	The request.bytes stores the loaded data.		*/
            this.textures[n].LoadImage(request.bytes);
        }
        else
        {
            Debug.LogWarning("Failed to load image file. (path=" + path + ")");
        }
    }

    public void OnGUI()
    {
       if (Scene_00_SampleList.ShowList == true)
        {
            return;
		}
		
		Scene_00_GUI.BeginGui("02/SampleSub");

		/* Set UI skin */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
        /*	Display the loaded texture data.	*/
        if (this.textures[0])
        {
            GUI.DrawTexture(new Rect(0, 200, this.textures[0].width, this.textures[0].height), this.textures[0]);
        }
        if (this.textures[1])
        {
            GUI.DrawTexture(new Rect(280, 255, this.textures[1].width, this.textures[1].height), this.textures[1]);
        }
        /*	Display the loaded text data.	*/
		GUIStyle style = new GUIStyle();
		style.normal.textColor = Color.black;
        GUI.Label(new Rect(10, 80, Screen.width, 200), this.loadedText, style);
		

		GUILayout.BeginArea(new Rect(Screen.width - 416, 70+50, 400, 800));
		
		if (Scene_00_GUI.Button("Load Text File"))
        {
            /*	Start the text data loading routine. (LoadTextFile function)	*/
            StartCoroutine(this.LoadTextFile(Path.Combine(CriWare.streamingAssetsPath, "sample_text.txt")));
        }
		if (Scene_00_GUI.Button("Load Image File (Local)"))
        {
            /*	Start the image data loading routine. (LoadImageFile function) (a local file)	*/
            StartCoroutine(this.LoadImageFile(Path.Combine(CriWare.streamingAssetsPath, "sample_image1.png"), 0));
        }
		if (Scene_00_SampleListData.supportNetworkAccess) {
			/*	Start the image data loading routine. (LoadImageFile function) (a file on the network)	*/
			if (Scene_00_GUI.Button("Load Image File (URL)"))
			{
				StartCoroutine(this.LoadImageFile("http://crimot.com/sdk/sampledata/crifilesystem/sample_image2.png", 1));
			}
		}
		
		GUILayout.Space(8);
        /*	Erase the displayed data.	*/
		if (Scene_00_GUI.Button("Reset"))
        {
            this.loadedText = "";
            this.textures[0] = null;
            this.textures[1] = null;
        }
		GUILayout.EndArea();

		Scene_00_GUI.EndGui();
    }
    #endregion
}
