﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;


/*	従来スタイル(ORDINARY_STYLE)による読み込み	*/
/*	Loading by the ordinary style (ORDINARY_STYLE)	*/
public class Scene_01_LoadFile_Ordinary : MonoBehaviour
{
    #region Variables
    /*	binder						*/
    private CriFsBinder binder = null;
    /*	loader						*/
    private CriFsLoader loader1 = null;	/*	for text		*/
    private CriFsLoader loader2 = null;	/*	for image 1		*/
    private CriFsLoader loader3 = null;	/*	for image 2		*/
    /*	read buffer	*/
    private byte[] buffer1 = null;		/*	for text		*/
    private byte[] buffer2 = null;		/*	for image 1		*/
    private byte[] buffer3 = null;		/*	for image 2		*/
    /*	text data				*/
    private string loadedText;
    /*	texture					*/
    private Texture2D[] textures = new Texture2D[2];
    #endregion

    #region Functions

	public void Awake() {
	    binder  = new CriFsBinder();
	    loader1 = new CriFsLoader();
	    loader2 = new CriFsLoader();
	    loader3 = new CriFsLoader();
	}
	
	/*	Executed when the componets are destroyed.	*/
	public void OnDestroy() {
		/*	Release the loaders and the resources.	*/
		loader1.Dispose();
		loader2.Dispose();
		loader3.Dispose();
		binder.Dispose();
	}

    /*	Execute the Update() once per frame.	*/
    public void Update()
    {
        /*	Check whether the text file is being loaded? */
        if (loader1.GetStatus() == CriFsLoader.Status.Complete) {
			/*	Store the text data in the buffer1.	*/
			using (var st = new StreamReader(new MemoryStream(buffer1), Encoding.UTF8)) {
				this.loadedText = st.ReadToEnd(); /*	Register the text data.	*/
			}	
            loader1.Stop();
        } else if (loader1.GetStatus() == CriFsLoader.Status.Error) {
			/*	An error occurred.	*/
            Debug.LogWarning("Failed to load a text file.");
            loader1.Stop();
        }
        /*	Check whether the image file is being loaded? */
        if (loader2.GetStatus() == CriFsLoader.Status.Complete) {
			/*	Store the image data in the buffer2.	*/
            this.textures[0] = new Texture2D(0, 0);
            this.textures[0].LoadImage(buffer2);		/*	Register to the texture.	*/
            loader2.Stop();
        } else if (loader2.GetStatus() == CriFsLoader.Status.Error) {
			/*	An error occurred.	*/
            Debug.LogWarning("Failed to load a image file.");
            loader2.Stop();
        }
        /*	Check whether the image file is being loaded? */
        if (loader3.GetStatus() == CriFsLoader.Status.Complete) {
			/*	Store the image data in the buffer3.	*/
            this.textures[1] = new Texture2D(0, 0);
            this.textures[1].LoadImage(buffer3);		/*	Register to the texture.	*/
            loader3.Stop();
        } else if (loader3.GetStatus() == CriFsLoader.Status.Error) {
			/*	An error occurred.	*/
            Debug.LogWarning("Failed to load a image file.");
            loader3.Stop();
        }
    }
    /*	The GUI control	*/
    public void OnGUI()
    {
        if (Scene_00_SampleList.ShowList == true)
        {
            return;
		}

		Scene_00_GUI.BeginGui("02/SampleSub");
		
		/* Set UI skin */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
        /*	Display the loaded texture data.	*/
        if (this.textures[0])
        {
            GUI.DrawTexture(new Rect(0, 200, this.textures[0].width, this.textures[0].height), this.textures[0]);
        }
        if (this.textures[1])
        {
            GUI.DrawTexture(new Rect(280, 255, this.textures[1].width, this.textures[1].height), this.textures[1]);
        }
        /*	Display the loaded text data.	*/
		GUIStyle style = new GUIStyle();
		style.normal.textColor = Color.black;
        GUI.Label(new Rect(10, 80, Screen.width, 200), this.loadedText, style);		
		
		
		GUILayout.BeginArea(new Rect(Screen.width - 416, 70+50, 400, 800));

        /*	Loading process #1 for the text file. (sample_text.txt)	*/
		if (Scene_00_GUI.Button("Load Text File"))
        {
            /*	Check whether the loader #1 is stopped.	*/
            if (loader1.GetStatus() == CriFsLoader.Status.Stop)
            {
                /*	Text file name	*/
                string path = Path.Combine(CriWare.streamingAssetsPath, "sample_text.txt");
                /*	Get the file size.	*/
                int file_size = (int)binder.GetFileSize(path);
                /*	If the file does not exist, the file_size is -1.	*/
                if (file_size > 0)
                {
                    /*	Allocate the read buffer #1.	*/
                    buffer1 = new byte[file_size];
                    /*	Issue a load request. (The third argument is the offset in the file.)	*/
                    loader1.Load(null, path, 0, file_size, buffer1);
                }
                else
                {
                    Debug.LogWarning("Failed to get file size. (path=" + path + ")");
                }
            }
        }
        /*	Loading process #1 for the image file. (sample_image1.png)	*/
		if (Scene_00_GUI.Button("Load Image File (Local)"))
        {
            /*	Check whether the loader #2 is stopped.		*/
            if (loader2.GetStatus() == CriFsLoader.Status.Stop)
            {
                /*	Image file name	*/
                string path = Path.Combine(CriWare.streamingAssetsPath, "sample_image1.png");
                /*	Get the file size.	*/
                int file_size = (int)binder.GetFileSize(path);
                /*	If the file does not exist, the file_size is -1.	*/
                if (file_size > 0)
                {
                    /*	Allocate the read buffer #2.	*/
                    buffer2 = new byte[file_size];
                    /*	Issue a load request. (The third argument is the offset in the file.)	*/
                    loader2.Load(null, path, 0, file_size, buffer2);
                }
                else
                {
                    Debug.LogWarning("Failed to get file size. (path=" + path + ")");
                }
            }
        }
        /*	Loading process #2 for the image file. (sample_image2.png)	*/
		if (Scene_00_SampleListData.supportNetworkAccess) {
			if (Scene_00_GUI.Button("Load Image File (URL)"))
			{
				/*	Check whether the loader #3 is stopped.	*/
				if (loader3.GetStatus() == CriFsLoader.Status.Stop)
				{
					/*	Image file name on the server (sample_image2.png)	*/
					string path = "http://crimot.com/sdk/sampledata/crifilesystem/sample_image2.png";
					/*	Get the file size.	*/
					int file_size = (int)binder.GetFileSize(path);
					/*	If not connected to the network, the file size is -1 for the file.	*/
					if (file_size > 0)
					{
						/*	Allocate the read buffer #3.	*/
						buffer3 = new byte[file_size];
						/*	Issue a load request. (The third argument is the offset in the file.)	*/
						loader3.Load(null, path, 0, file_size, buffer3);
					}
					else
					{
						Debug.LogWarning("Failed to get file size. (path=" + path + ")");
					}
				}
			}
		}
		GUILayout.Space(8);

		/*	Stop the loading.		*/
		if (Scene_00_GUI.Button("Stop Loading"))
        {
            this.loader1.Stop();
            this.loader2.Stop();
            this.loader3.Stop();
        }
		GUILayout.Space(8);

        /*	Erase the displayed data.	*/
		if (Scene_00_GUI.Button("Reset"))
        {
            this.loadedText = "";
            this.textures[0] = null;
            this.textures[1] = null;
        }
		
		GUILayout.EndArea();
		
		Scene_00_GUI.EndGui();
    }
    #endregion

}
