﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、CRI File SystemのユーティリティAPIを使い、
 * ローカル、またはリモートホスト上のCPKファイルをバインドした後、
 * CPKファイル内の単体ファイルを読み込みます。
 * 単体ファイルを読み込む際は、CPKをバインドしたバインダハンドルを参照し、
 * CPK内でのファイル名でアクセスします。
 *
 * 単体ファイルを読み込むステップでは、CPKファイルがローカル由来なのか、
 * リモート由来なのかを意識する必要がありません。
 */
/*EN
 * This sample loads a single (stand-alone) file in a CPK file after binding 
 * a CPK file that exists on the local storage or on the remote host by using
 * the Utility API of CRI File System.
 * To load a single file, access the file with the content file name in the CPK,
 * referencing the binder handle to which the CPK is bound.
 * In the steps of loading a single file, there is no need to consider 
 * whether the CPK file is put on the local storage or on the remote host.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class Scene_02_BindCpk : MonoBehaviour
{
	#region Variables
	/* path for a local CPK file (which is made at runtime) */
	private string localCpkFilePath;
	/* path(URL) for a CPK file on the remote host (which is made at runtime) */
	private string remoteCpkFilePath;
	
	/* file name to load the content file from the CPK */
	private const string ImageFilename = "criware.png";
	
	/* binder handle */
	private CriFsBinder binder = null;
	/* binder ID */
	private uint bindId = 0;
	
	/* storage for the loaded data */
	private Texture2D texture;
	private string loadedText;
	
	/* index for the application to store the status, */
	/* on which side the CPK file is bound, local storage or remote host. */
	private int bindCursor = 0;
	
	/* lock flag for the GUI button */
	private bool lockBindCpkButton = false;
	private bool lockLoadImageButton = false;
	private bool lockLoadTextButton = false;
	#endregion
	
	#region Variables

	/* The initialization process of the scene */
	void OnEnable()
	{	
		/* Create a binder. */
		/* 
		 * To use the binding feature such as file binding, CPK binding and directory binding,
		 * the application needs to explicitly create a binder.
		 */
		this.binder = new CriFsBinder();

		/* Make a path for a local CPK file. */
		this.localCpkFilePath = Path.Combine(CriWare.streamingAssetsPath, "sample.cpk");
		/* Make a path(URL) for a CPK file on the remote host. */
		this.remoteCpkFilePath = Path.Combine("http://crimot.com/sdk/sampledata/crifilesystem/", "sample.cpk");
	}

	/* The finalization process of the scene */
	void OnDisable()
	{
		/* Reset the scene. */
		this.reset();

		/* Perform the unbound processing. */
		this.UnbindCpk();
		
		/* Destroy the binder. */
		/* The application needs to explicitly destroy the binder when finished.*/
		this.binder.Dispose();
		this.binder = null;
	}
	
	/* The binding process of CPK file */
	private IEnumerator BindCpk(string path, int cursor)
	{
		/* Release the bound CPK. */
		this.UnbindCpk();
		
		this.lockBindCpkButton = true;
		
		/* Request a CPK binding. */
		/* Assign the content of the CPK to the specified binder handle. */
		var request = CriFsUtility.BindCpk(this.binder, path);

		/* Store the binder ID on the application side. */
		/* The binder ID is an ID to uniquely indentify a file binding (CPK for this sample). */
		/* It is used for the release processing etc. */
		this.bindId = request.bindId;

		/* Wait for the completion of binding. */
		yield return request.WaitForDone(this);

		this.lockBindCpkButton = false;

		if (request.error == null) {
			this.bindCursor = cursor;
			Debug.Log("Completed to bind CPK. (path=" + path + ")");
		} else {
			/* If failed, execute Unbind process. */
			this.UnbindCpk();
			Debug.LogWarning("Failed to bind CPK. (path=" + path + ")");
		}
	}
	
	/* Release the bound CPK. */
	private void UnbindCpk()
	{
		if (this.bindId > 0) {
			/* Release the bound CPK. */
			CriFsBinder.Unbind(this.bindId);
			this.bindId = 0;
			this.bindCursor = 0;
		}
	}
	
	/* The loading process of image file */
	private IEnumerator LoadImageFile(string path)
	{
		this.lockLoadImageButton = true;
		
		/* Issue a request for the file loading to the CPK file-bound binder. */
		var request = CriFsUtility.LoadFile(this.binder, path);
		/* Wait for the completion of loading. */
		yield return request.WaitForDone(this);

		this.lockLoadImageButton = false;

		if (request.error == null) {
			/* If successful, the texture data is created from the byte string. */
			this.texture = new Texture2D(0, 0);
			this.texture.LoadImage(request.bytes);
		} else {
			Debug.LogWarning("Failed to load image. (path=" + path + ")");
		}
	}
	
	/* The loading process of text file */
	private IEnumerator LoadTextFile(string path)
	{
		this.lockLoadTextButton = true;
		
		/* Issue a request for the file loading to the directory-bound binder. */
		var request = CriFsUtility.LoadFile(this.binder, path);
		/* Wait for the completion of loading. */
		yield return request.WaitForDone(this);

		this.lockLoadTextButton = false;

		if (request.error == null) {
			/* If successful, the text data is created from the byte string. */
			using (var st = new StreamReader(new MemoryStream(request.bytes), Encoding.UTF8)) {
				this.loadedText = st.ReadToEnd();
			}
		} else {
			Debug.LogWarning("Failed to load text. (path=" + path + ")");
		}
	}	
	/* Reset the scene. */
	private void reset()
	{
		if (this.texture) {
			/* Destroy the texture. */
			this.texture = null;
		}

		this.loadedText = "";
		
		/* Release the CPK. */
		this.UnbindCpk();
	}

	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}

		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		if (this.texture != null) {
			/* Display the texture if it has already been loaded. */
			GUI.DrawTexture(new Rect(0, 250, this.texture.width, this.texture.height), this.texture);
		}
		
		GUILayout.BeginArea(new Rect(Screen.width - 416, 70 , 400, 320));
		
		GUILayout.Space(8);
		GUILayout.Label("Step1 : Bind CPK File");
		if (this.lockBindCpkButton == false) {
			if (Scene_00_GUI.Button(((this.bindCursor == 1) ? "*" : " ") + " Bind CPK File (Local)")) {
				/* Start the binding process of the local CPK file. */
				StartCoroutine(this.BindCpk(this.localCpkFilePath, 1));
			}
		} else {
			Scene_00_GUI.Button("Binding...");
		}
		if (Scene_00_SampleListData.supportNetworkAccess) {
			if (this.lockBindCpkButton == false) {
				if (Scene_00_GUI.Button(((this.bindCursor == 2) ? "*" : " ") + " Bind CPK File (URL)")) {
					/* Start the binding process of the remote CPK file. */
					StartCoroutine(this.BindCpk(this.remoteCpkFilePath, 2));
				}
			} else {
				Scene_00_GUI.Button("Binding...");
			}
		}
		GUILayout.Space(8);
		GUILayout.Label("Step2 : Load File");
		GUI.enabled = (this.bindId > 0) ? true : false;
		/* The GUI button for the file loading is enabled if the CPK file has already been bound. */
		if (this.lockLoadImageButton == false) {
			if (Scene_00_GUI.Button("Load Image File")) {
				/* Start the loading process of the image file on the CPK. */
				StartCoroutine(this.LoadImageFile(ImageFilename));
			}
		} else {
			Scene_00_GUI.Button("Loading...");
		}

		if (this.lockLoadTextButton == false) {
			if (Scene_00_GUI.Button("Load Text File")) {
				/* Start the loading process of the text file in the directory. */
				StartCoroutine(this.LoadTextFile("sample_text.txt"));
			}
		} else {
			/* Disable the GUI button during loading. */
			Scene_00_GUI.Button("Loading...");
		}		
		
		GUILayout.Space(8);

		GUI.enabled = true;
		/* Reset the scene. */
		if (Scene_00_GUI.Button("Reset")) {
			this.reset();
		}
		
		GUILayout.EndArea();
        GUI.Label(new Rect(10, 80, Screen.width, 200), this.loadedText);
		
		Scene_00_GUI.EndGui();
	}
	#endregion
}

/* end of file */
