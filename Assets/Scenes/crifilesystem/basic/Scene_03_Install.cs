﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、CRI File SystemのユーティリティAPIを使い、
 * モートホスト上のCPKファイル、画像ファイルをローカルのファイルシステム上に
 * インストールします。
 * その後、インストールしたローカルファイルを読み込みます。
 * ローカルの画像ファイルはテクスチャとして直接ロードします。
 * ローカルのCPKファイルからはtxtファイルをロードします。
 *
 * 注意）
 * 一度インストールしたデータは、アプリ側が明示的に削除しない限り、
 * ローカルのファイルシステム上に残り続けます。
 * そのため、本サンプルの２回目以降の実行では、
 * インストールされたローカルファイルを削除しない限り、
 * インストール実行前でもCPKのバインド処理等が成功し、ファイルをロードできます。
 */
/*EN
 * This sample installs a CPK file and an image file on the remote host 
 * to the local file system by using the Utility API of CRI File System, 
 * and then loads the installed local files.
 * The local image file is directly loaded as a texture, and the text file
 * is loaded from the local CPK file.
 *
 * [Note]
 * The data that has been installed once remains on the local file system 
 * unless it is explicitly deleted on the application side.
 * Therefore, at the second execution or more, the CPK binding succeeds 
 * even before the installation, and files can be loaded.
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class Scene_03_Install : MonoBehaviour
{
	#region Variables
	private const string RemoteUrl = "http://crimot.com/sdk/sampledata/crifilesystem/";
	private const string ImageFilename = "criware.png";
	private const string CpkFilename = "sample.cpk";
	
	/* path for a local CPK file (which is made at runtime) */
	private string localCpkPath;
	/* path for a local image file (which is made at runtime) */
	private string localImagePath;
	/* path(URL) for a CPK file on the remote host (which is made at runtime) */
	private string remoteCpkPath;
	/* path(URL) for an image file on the remote host (which is made at runtime) */
	private string remoteImagePath;
	
	/* storage area for the installation request */
	private CriFsInstallRequest[] installRequest = new CriFsInstallRequest[2];

	/* parameter for CPK binding */
	private CriFsBinder binder = null;
	private uint bindId = 0;

	/* parameter for storing the loaded file */
	private Texture2D texture;
	private string loadedText;

	/* lock flag for the GUI button */
	private bool lockBindCpkButton = false;
	private bool lockLoadImageButton = false;
	private bool lockLoadTextButton = false;
	#endregion
	
	#region Variables
	void OnEnable()
	{
		/* Create a binder */
		/*
		 * To use the binding feature such as file binding, CPK binding and directory binding,
		 * the application needs to explicitly create a binder.
		 */
		this.binder = new CriFsBinder();
		
		/* Make a path */
		/* Make a path for the destination location of the installation. */
		this.localCpkPath = Path.Combine(CriWare.installTargetPath, CpkFilename);
		this.localImagePath = Path.Combine(CriWare.installTargetPath, ImageFilename);

		/* Make a path for the source location of the installation. */
		this.remoteCpkPath = Path.Combine(RemoteUrl, CpkFilename);
		this.remoteImagePath = Path.Combine(RemoteUrl, ImageFilename);
	}
	
	void OnDisable()
	{
		/* Reset the scene. */
		this.reset();

		/* Perform the unbound processing. */
		this.UnbindCpk();
		
		/* Destroy the binder. */
		/* The application needs to explicitly destroy the binder when finished.*/
		this.binder.Dispose();
		this.binder = null;
	}

	private IEnumerator Install(string src_path, string install_path, int request_index)
	{
		/* Request for the installation */
		this.installRequest[request_index] = CriFsUtility.Install(null, src_path, install_path);
		/* Wait untill the installation is completed. */
		yield return this.installRequest[request_index].WaitForDone(this);
		/* [Note] 
		 * The request for the installation is stored on the application side.
		 * It is also used for a flag to avoid multiple installations.
		 */
		
		if (this.installRequest[request_index].error == null) {
			Debug.Log("Completed installation." + src_path);
		} else {
			Debug.LogWarning("Failed installation." + src_path);
		}
		/* Release the request after the installation is completed. */
		this.installRequest[request_index] = null;
	}
	
	private IEnumerator BindCpk(string path)
	{
		this.UnbindCpk();
		
		this.lockBindCpkButton = true;
		var request = CriFsUtility.BindCpk(this.binder, path);
		this.bindId = request.bindId;
		yield return request.WaitForDone(this);
		this.lockBindCpkButton = false;

		if (request.error == null) {
			Debug.Log("Completed to bind CPK.");
		} else {
			this.UnbindCpk();
			Debug.LogWarning("Failed to bind CPK.");
		}
	}

	/* Release the bound CPK. */
	private void UnbindCpk()
	{
		if (this.bindId > 0) {
			/* Release the bound CPK. */
			CriFsBinder.Unbind(this.bindId);
			this.bindId = 0;
		}
	}
	
	/* The loading process of image file */
	private IEnumerator LoadImageFile(string path)
	{
		this.lockLoadImageButton = true;
		
		/* Issue a request for the file loading. */
		var request = CriFsUtility.LoadFile(path);
		/* Wait for the completion of loading. */
		yield return request.WaitForDone(this);

		this.lockLoadImageButton = false;

		if (request.error == null) {
			/* If successful, the texture data is created from the byte string. */
			this.texture = new Texture2D(0, 0);
			this.texture.LoadImage(request.bytes);
		} else {
			Debug.LogWarning("Failed to load image. (path=" + path + ")");
		}
	}
	
	/* The loading process of text file */
	private IEnumerator LoadTextFile(string path)
	{
		this.lockLoadTextButton = true;
		
		/* Issue a request for the file loading to the directory-bound binder. */
		var request = CriFsUtility.LoadFile(this.binder, path);
		/* Wait for the completion of loading. */
		yield return request.WaitForDone(this);

		this.lockLoadTextButton = false;

		if (request.error == null) {
			/* If successful, the text data is created from the byte string. */
			using (var st = new StreamReader(new MemoryStream(request.bytes), Encoding.UTF8)) {
				this.loadedText = st.ReadToEnd();
			}
		} else {
			Debug.LogWarning("Failed to load text. (path=" + path + ")");
		}
	}
	
	private void reset()
	{
		/* Stop the installation. */  
		foreach (CriFsInstallRequest req in this.installRequest) {
			if (req != null) {
				req.Stop();	
			}
		}

		if (this.texture) {
			/* Destroy the texture. */
			this.texture = null;
		}
		
		this.loadedText = "";

		/* Release the CPK */
		this.UnbindCpk();
	}

	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");
		
		/* Set UI skin */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		if (this.texture != null) {
			GUI.DrawTexture(new Rect(0, 250, this.texture.width, this.texture.height), this.texture);
		}

		GUILayout.BeginArea(new Rect(Screen.width - 416, 60 , 400, 400));
		
		GUILayout.Space(8);
		
		GUILayout.Label("Step1 : Install from HTTP Server");
		if (this.installRequest[0] == null) {
			if (Scene_00_GUI.Button("Install CPK File")) {
				/* Unbound the CPK file before the installation because it is overwritten. */
				this.UnbindCpk();
				/* Start the installation process of the CPK file. */
				StartCoroutine(this.Install(this.remoteCpkPath, this.localCpkPath, 0));
			}
		} else {
			/* Get the progress from the request for the installation, and display it on the screen. */
			float progress = this.installRequest[0].progress;
			GUILayout.Label("Installing... " + (int)(progress * 100) + "%");
			GUILayout.Box("", GUILayout.Width(390 * progress + 10), GUILayout.Height(8));
		}
		if (this.installRequest[1] == null) {
			if (Scene_00_GUI.Button("Install Image File")) {
				/* Start the installation process of the image file. */
				StartCoroutine(this.Install(this.remoteImagePath, this.localImagePath, 1));
			}
		} else {
			/* Get the progress from the request for the installation, and display it on the screen. */
			float progress = this.installRequest[1].progress;
			GUILayout.Label("Installing... " + (int)(progress * 100) + "%");
			GUILayout.Box("", GUILayout.Width(390 * progress + 10), GUILayout.Height(8));
		}

		GUILayout.Space(8);
		GUILayout.Label("Step2 : Bind Installed CPK");
		if (this.lockBindCpkButton == false) {
			if (Scene_00_GUI.Button(((this.bindId > 0) ? "* " : "") + "Bind CPK File")) {
				/* Start the binding process of the CPK file that is installed on the local storage. */
				StartCoroutine(this.BindCpk(this.localCpkPath));
			}
		} else {
			Scene_00_GUI.Button("Binding...");
		}

		GUILayout.Space(8);
		GUILayout.Label("Step3 : Load Data");
		if (this.lockLoadImageButton == false) {
			if (Scene_00_GUI.Button("Load Image from File")) {
				/* Start the loading process of the image file. */
				StartCoroutine(this.LoadImageFile(this.localImagePath));
			}
		} else {
			Scene_00_GUI.Button("Loading...");
		}

		if (this.lockLoadTextButton == false) {
			if (Scene_00_GUI.Button("Load Text File")) {
				/* Start the loading process of the text file in the directory. */
				StartCoroutine(this.LoadTextFile("sample_text.txt"));
			}
		} else {
			/* Disable the GUI button during loading. */
			Scene_00_GUI.Button("Loading...");
		}		
				
		GUILayout.Space(8);
		if (Scene_00_GUI.Button("Reset")) {
			/* Reset the scene. */
			this.reset();
		}
		
		GUILayout.EndArea();
        GUI.Label(new Rect(10, 80, Screen.width, 200), this.loadedText);
		
		Scene_00_GUI.EndGui();
	}
	#endregion
}

/* end of file */
