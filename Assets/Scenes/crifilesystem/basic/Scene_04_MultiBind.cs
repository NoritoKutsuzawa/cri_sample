﻿/****************************************************************************
 *
 * Copyright (c) 2012 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、CRI File SystemのユーティリティAPIを使い、１つのバインダに
 * 対して、ローカルのCPKファイルとリモートホスト上のCPKファイルをバインドし
 * ます。
 * 
 * 複数のCPKファイルを一つのバインダにバインドする事で、ファイルのロード時
 * には、バインドされている複数のCPK内を指定されたファイル名で検索してファイ
 * ルをロードします。
 * 
 * このように、１つのバインダに対して複数の単体ファイル、CPKファイル、ディ
 * レクトリをバインドする事を、「マルチバインド」と呼びます。
 * マルチバインドにより、複数のバインダをあたかも１つであるかのようにファン
 * ル参照することが可能になります。
 * 
 * 本サンプルでは、リモートホスト上のCPKファイルをローカルCPKファイルのアッ
 * プデートパック(差分ファイル)と見立てています。
 * ローカルCPKファイルには本サンプルで使用する全てのファイルが包含されていま
 * すが、リモートホスト上のCPKファイルには、更新のあった（という想定の）一部
 * のファイルが含まれています。
 *
 * ファイルのロード時には、アップデートパック(差分ファイル)側のCPKを優先して
 * 参照する必要があるため、それぞれのCPKバインドIDに対し、
 * CriFsBinder.SetPriority()による優先度設定を行っています。
 * 
 * 本サンプルのように、ローカルCPKにアプリで必要な全てのファイルを含めておき、
 * 各ファイルの更新分をアップデート側CPKにパッキングして後からマルチバインド
 * する事で、アプリケーションのロード部では元々あったファイル、更新されたファ
 * イルを特に意識せずに同じコードでロードすることができます。
 * 
 * アップデートパック側CPKファイルには、更新された「sample_image2.png」のみ
 * 含まれています。
 * 従って、他のファイルにアクセスした場合は、ローカルCPKからファイルを読み込
 * みます。
 */
/*EN
 *  This sample uses the utility API of the CRI File System to bind local and 
 *  remote CPK files into a single binder.
 *
 *  For details about this sample, please see the CRIWARE Unity Plug-in Manual
 *    "Samples"->"Samples for FileMajik PRO"->"[CriFs] Multi-binding"
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Scene_04_MultiBind : MonoBehaviour
{
	#region Variables
	/* path for a local CPK file (which is made at runtime) */
	private string localCpkFilePath;
	/* path(URL) for a CPK file on the remote host (which is made at runtime) */
	private string updateCpkFilePath;
	
	/* storage for the loaded data */
	private Texture2D [] textures = new Texture2D[2];

	/* binder handle */
	private CriFsBinder binder = null;
	private uint[] bindId = new uint[2] {0, 0};
	private int bindCount = 0;

	/* lock flag for the GUI button */
	private bool lockBindCpkButton = false;
	private bool[] lockLoadImageButton = new bool[2] {false, false};
	#endregion
	
	#region Variables
	/* The initialization process of the scene */
	void OnEnable()
	{
		/* Create a binder. */
		/* 
		 * To use the binding feature such as file binding, CPK binding and directory binding,
		 * the application needs to explicitly create a binder.
		 */
		this.binder = new CriFsBinder();
		
		/* Make a path for a local CPK file. */
		this.localCpkFilePath = Path.Combine(CriWare.streamingAssetsPath, "sample.cpk");
		/* Make a path(URL) for a CPK file on the remote host. */
		this.updateCpkFilePath = Path.Combine("http://crimot.com/sdk/sampledata/crifilesystem/", "update.cpk");
	}
	
	/* The finalization process of the scene */
	void OnDisable()
	{
		/* Perform the unbound processing. */
		this.UnbindCpk(0);
		this.UnbindCpk(1);
		
		/* Destroy the binder. */
		/* The application needs to explicitly destroy the binder when finished.*/
		this.binder.Dispose();
		this.binder = null;
	}
	
	/* The multi-binding process of CPK file */
	private IEnumerator MultiBindCpk(string path, int bind_index)
	{
		/* Release the bound CPK. */
		this.UnbindCpk(bind_index);
		
		this.lockBindCpkButton = true;
		
		/* Request a CPK binding. */
		/* Assign the content of the CPK to the specified binder handle. */
		var request = CriFsUtility.BindCpk(this.binder, null, path);
		
		/* Store the binder ID on the application side. */
		/* It is used for the release processing etc. */
		this.bindId[bind_index] = request.bindId;

		/* Wait for the completion of binding. */
		yield return request.WaitForDone(this);

		this.lockBindCpkButton = false;

		if (request.error == null) {
			/* Assign the search priority.
			/*JP
			 * 検索優先度は、値が大きいほど高くなります。
			 * 本サンプルでは常に、アップデートパック側CPKをローカルのCPKより先に検索するように設定しています。 
			 */
			/*EN
			 * The larger the value is , the higher the priority is.
			 * This sample always searches the updated CPK before the local CPK.
			 */
			CriFsBinder.SetPriority(this.bindId[bind_index], bind_index);

			/*JP
			 * 一つでもCPKがバインド済みならGUI側のファイルロードボタンを有効化するために
			 * バインド回数をカウントアップ
			 */
			/*EN
			 * Decrement the binder counter to enable the GUI button for file loading
			 * if even one CPK file has been bound.
			 */
			this.bindCount++;
			Debug.Log("Completed to bind CPK. (path=" + path + ")");
		} else {
			/* If failed, execute Unbind process. */
			this.UnbindCpk(bind_index);
			Debug.LogWarning("Failed to bind CPK. (path=" + path + ")");
		}
	}
	
	/* Release the bound CPK */
	private void UnbindCpk(int bind_index)
	{
		if (this.bindId[bind_index] > 0) {
			/* Release the bound CPK. */
			CriFsBinder.Unbind(this.bindId[bind_index]);
			this.bindId[bind_index] = 0;
			/* Decrement the binder counter. */
			this.bindCount--;
		}
	}

	/* The loading process of image file */
	private IEnumerator LoadImageFile(string path, int load_index)
	{
		this.lockLoadImageButton[load_index] = true;

		/* Issue a request for the file loading to the CPK-bound binder. */
		var request = CriFsUtility.LoadFile(this.binder, path);
		/* Wait for the completion of loading. */
		yield return request.WaitForDone(this);

		this.lockLoadImageButton[load_index] = false;

		if (request.error == null) {
			/* If successful, the texture data is created from the byte string. */
			this.textures[load_index] = new Texture2D(0, 0);
			this.textures[load_index].LoadImage(request.bytes);
		} else {
			Debug.LogWarning("Failed to load image. (path=" + path + ")");
		}
	}


	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");
		
		/* Set UI skin */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		for (int i = 0; i < 2; i++) {
			/* Display the loaded images in a single horizontal row. */
			if (this.textures[i]) {
				GUI.DrawTexture(new Rect((320 * i), 90, this.textures[i].width, this.textures[i].height), this.textures[i]);
			}
		}

		GUILayout.BeginArea(new Rect(Screen.width - 416, 60 , 400, 400));

		GUILayout.Label("Step1 : Multi Bind CPK File");

		if(this.bindId[0] == 0) {
			if (this.lockBindCpkButton == false) {
				if (Scene_00_GUI.Button("Bind CPK File (Local)")) {
					/* Start the binding process of a local CPK file. */
					StartCoroutine(this.MultiBindCpk(this.localCpkFilePath, 0));
				}
			} else {
				/* Disable the GUI button so that multiple binding is not executed during binding. */
				Scene_00_GUI.Button("Binding...");
			}
		} else {
			/* Display the button for unbinding if it has already been bound. */
			if (Scene_00_GUI.Button("Unbind CPK File (Local)")) {
				/*  Execute the unbound processing. */
				this.UnbindCpk(0);
			}
		}
		
		if(this.bindId[1] == 0) {
			if (this.lockBindCpkButton == false) {
				if (Scene_00_GUI.Button("Bind Update CPK File (URL)")) {
					/* Start the binding process of a CPK file for the update pack. */
					StartCoroutine(this.MultiBindCpk(this.updateCpkFilePath, 1));
				}
			} else {
				/* Disable the GUI button so that multiple binding is not executed during binding. */
				Scene_00_GUI.Button("Binding...");
			}
		} else {
			/* Display the button for unbinding if it has already been bound. */
			if (Scene_00_GUI.Button("Unbind CPK File (URL)")) {
				/*  Execute the unbound processing. */
				this.UnbindCpk(1);
			}
		}

		GUILayout.Space(8);
		/* Load the image file in the CPK. */
		GUILayout.Label("Step2 : Load File");
		if (this.lockLoadImageButton[0] == false) {
			GUI.enabled = (bindCount > 0) ? true : false;
			if (Scene_00_GUI.Button("Load Image File 1")) {
				/* Start the loading process of the image file in the CPK. */
				/*JP
				 * アップデートパック側に更新後のファイルが存在しないので
				 * 常にローカルCPK内の同一画像ファイルが読み込まれる
				 */
				/*EN
				 * The same image file in the local CPK is always loaded 
				 * because an updated file does not exist in the update pack.
				 */
				StartCoroutine(this.LoadImageFile("sample_image1.png", 0));
			}
		} else {
			/* Disable the GUI button so that multiple loading is not executed during loading. */
			Scene_00_GUI.Button("Loading...");
		}
		if (this.lockLoadImageButton[1] == false) {
			GUI.enabled = (bindCount > 0) ? true : false;
			if (Scene_00_GUI.Button("Load Image File 2")) {
				/* Start the loading process of the image file in the CPK. */
				/*JP
				 * こちらの画像ファイルは、マルチバインドの状況に応じて更新前か更新後の画像が読み込まれる
				 */
				/*EN
				 * Either the image file before the update or after the update is loaded depending on the multi-binding status.
				 */
				StartCoroutine(this.LoadImageFile("sample_image2.png", 1));
			}
		} else {
			/* Disable the GUI button during loading. */
			Scene_00_GUI.Button("Loading...");
		}

		GUILayout.Space(8);
		GUI.enabled = true;
		if (Scene_00_GUI.Button("Reset Image")) {
			/* Destroy the texture for displaying the image file loaded. */
			for (int i = 0; i < 2; i++) {
				if (this.textures[i]) {
					this.textures[i] = null;
				}
			}
		}
		
		GUILayout.EndArea();

		Scene_00_GUI.EndGui();
	}
	#endregion
}

/* end of file */
