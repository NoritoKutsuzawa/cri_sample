﻿/****************************************************************************
 *
 * Copyright (c) 2012 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルでは、バインドディレクトリ機能について説明します。
 *
 * バインドディレクトリ機能は、指定したディレクトリ以下をバインドする機能です。
 * しかしながら、バインドディレクトリ時にはファイルのオープンなどは行われず、
 * ファイルロード等、実際のファイルアクセス時にファイルオープン処理やファイル
 * サイズ取得が行われます。
 * 予めファイルオープン処理やファイル情報の取得を行う事のできるCPKファイルを
 * 用いた動作と比較すると、とても効率の悪い処理となります。
 * 
 * 上記の理由から、開発中などでは特に問題にはなりませんが、リリース前など、
 * 最終的には効率の良いCPKファイルにパッキングしてご利用頂くことを推奨します。
 * 
 * 本サンプルでは、StreamingAssetsディレクトリと、リモートホスト上のディレク
 * トリ（URL指定）をマルチバインドしています。
 * StreamingAssets内には開発初期に用意したデータファイルが置かれており、データ
 * ファイルの更新はリモートホスト上に対して行われていくといような利用方法を
 * 想定しています。
 * 
 * データの作成者は最新ファイルをリモートホスト上に置くだけで、ゲーム本体側は
 * 一切変更せずにデータファイルを更新できるということになります。
 */
/*EN
 *  This sample uses the directory binding feature.
 *
 *  For details about this sample, please see the CRIWARE Unity Plug-in Manual
 *    "Samples"->"Samples for FileMajik PRO"->"[CriFs] Directory binding"
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class Scene_05_BindDirectory : MonoBehaviour
{
	#region Variables
	/* path for a local directory (which is made at runtime) */
	private string localDirectoryPath;
	/* path(URL) for a directory on the remote host (which is made at runtime) */
	private string remoteDirectoryPath;
	
	/* binder handle */
	private CriFsBinder binder = null;
	private uint[] bindId = new uint[2] {0, 0};
	private int bindCount = 0;

	/* storage for the loaded data */
	private Texture2D [] textures = new Texture2D[2];
	private string loadedText;
	
	/* lock flag for the GUI button */
	private bool lockBindDirectoryButton = false;
	private bool lockLoadTextButton = false;
	private bool[] lockLoadImageButton = new bool[2] {false, false};
	
	#endregion
	
	#region Variables
	
	/* The binding process of directory */
	void OnEnable()
	{
		/* Create a binder. */
		this.binder = new CriFsBinder();
		
		/* Make a path for a local directory. */
		this.localDirectoryPath = CriWare.streamingAssetsPath;
		/* Make a path for a directory on the remote host. */
		this.remoteDirectoryPath = "http://crimot.com/sdk/sampledata/crifilesystem/";
		
	}
	
	void OnDisable()
	{
		/* Perform the unbound processing. */
		this.UnbindCpk(0);
		this.UnbindCpk(1);
		
		/* Destroy the binder. */
		this.binder.Dispose();
		this.binder = null;
	}
	
	private IEnumerator BindDirectory(string path, int bind_index)
	{
		/* Release the bound CPK. */
		this.UnbindCpk(bind_index);
	
		this.lockBindDirectoryButton = true;

		/* Request a directory binding. */
		/*JP
		 * 実際にはディレクトリをマルチバインドします。
		 * マルチバインドについては Scene_05_MultiBindを参考にしてください。
		 */
		/*EN
		 *  Actually, the multi-binding of directory is performed.
		 *  For details about multi-binding, please see Scene_04_MultiBind.
		 */
		var request = CriFsUtility.BindDirectory(this.binder, null, path);

		/* Store the binder ID on the application side. */
		this.bindId[bind_index] = request.bindId;
		
		/* Wait for the completion of directory binding. */
		yield return request.WaitForDone(this);

		this.lockBindDirectoryButton = false;

		if (request.error == null) {
			/*JP
			 * リモートホスト側の優先度を高く設定
			 * リモートホスト側に更新ファイルがあれば、そちらが優先的にロードされるようになる
			 */
			/*EN
			 * Set the higher priority to the remote host. 
			 * If the updated files exist on the remote host, they are preferentially loaded.
			 */
			CriFsBinder.SetPriority(this.bindId[bind_index], bind_index);
			
			/*JP
			 * 一つでもCPKがバインド済みならGUI側のファイルロードボタンを有効化するために
			 * バインド回数をカウントアップ
			 */
			/*EN
			 * Decrement the binder counter to enable the GUI button for file loading
			 * if even one CPK file has been bound.
			 */
			this.bindCount++;
			Debug.Log("Completed to bind Directory. (path=" + path + ")");
		} else {
			/* If failed, execute Unbind process. */
			this.UnbindCpk(bind_index);
			Debug.LogWarning("Failed to bind Directory. (path=" + path + ")");
		}
	}
	
	/* Release the bound CPK */
	private void UnbindCpk(int bind_index)
	{
		if (this.bindId[bind_index] > 0) {
			/* Release the bound CPK. */
			CriFsBinder.Unbind(this.bindId[bind_index]);
			this.bindId[bind_index] = 0;
			this.bindCount--;
		}
	}
	
	/* The loading process of text file */
	private IEnumerator LoadTextFile(string path)
	{
		this.lockLoadTextButton = true;
		
		/* Issue a request for the file loading to the directory-bound binder. */
		var request = CriFsUtility.LoadFile(this.binder, path);
		/* Wait for the completion of loading. */
		yield return request.WaitForDone(this);

		this.lockLoadTextButton = false;

		if (request.error == null) {
			/* If successful, the text data is created from the byte string. */
			using (var st = new StreamReader(new MemoryStream(request.bytes), Encoding.UTF8)) {
				this.loadedText = st.ReadToEnd();
			}
		} else {
			Debug.LogWarning("Failed to load text. (path=" + path + ")");
		}
	}
	
	/* The loading process of image file */
	private IEnumerator LoadImageFile(string path, int load_index)
	{
		this.lockLoadImageButton[load_index] = true;

		/* Issue a request for the file loading to the directory-bound binder. */
		var request = CriFsUtility.LoadFile(this.binder, path);
		/* Wait for the completion of loading. */
		yield return request.WaitForDone(this);

		this.lockLoadImageButton[load_index] = false;

		if (request.error == null) {
			/* If successful, the texture data is created from the byte string. */
			this.textures[load_index] = new Texture2D(0, 0);
			this.textures[load_index].LoadImage(request.bytes);
		} else {
			Debug.LogWarning("Failed to load image. (path=" + path + ")");
		}
	}
	
	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");
		
		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;

		for (int i = 0; i < 2; i++) {
			/* Display the loaded images in a single horizontal row. */
			if (this.textures[i]) {
				GUI.DrawTexture(new Rect((320 * i), 90, this.textures[i].width, this.textures[i].height), this.textures[i]);
			}
		}
		/* Display the loaded text. */
		GUIStyle style = new GUIStyle();
		style.normal.textColor = Color.black;
        GUI.Label(new Rect(10, 80, Screen.width, 200), this.loadedText, style);
		
		GUILayout.BeginArea(new Rect(Screen.width - 416, 60 , 400, 400));

		GUILayout.Label("Step1 : Bind Directory");

		if(this.bindId[0] == 0) {
			if (this.lockBindDirectoryButton == false) {
				if (Scene_00_GUI.Button("Bind Directory (Local)")) {
					/* Start the binding process of the local directory. */
					StartCoroutine(this.BindDirectory(this.localDirectoryPath, 0));
				}
			} else {
				/* Disable the GUI button during binding. */
				Scene_00_GUI.Button("Binding...");
			}
		} else {
			/* Display the GUI button for unbinding if it has already been bound. */
			if (Scene_00_GUI.Button("Unbind Directory (Local)")) {
				this.UnbindCpk(0);
			}
		}
		
		if (Scene_00_SampleListData.supportNetworkAccess) {
			if(this.bindId[1] == 0) {
				if (this.lockBindDirectoryButton == false) {
					if (Scene_00_GUI.Button("Bind Directory (URL)")) {
						/* Start the binding process of the directory on the remote host. */
						StartCoroutine(this.BindDirectory(this.remoteDirectoryPath, 1));
					}
				} else {
					/* Disable the GUI button during binding. */
					Scene_00_GUI.Button("Binding...");
				}
			} else {
				/* Display the GUI button for unbinding if it has already been bound. */
				if (Scene_00_GUI.Button("Unbind Directory (URL)")) {
					this.UnbindCpk(1);
				}
			}
		}

		GUILayout.Space(8);
		GUILayout.Label("Step2 : Load File");
		
		if (this.lockLoadTextButton == false) {
			GUI.enabled = (bindCount > 0) ? true : false;
			if (Scene_00_GUI.Button("Load Text File")) {
				/* Start the loading process of the text file in the directory. */
				StartCoroutine(this.LoadTextFile("sample_text.txt"));
			}
		} else {
			/* Disable the GUI button during loading. */
			Scene_00_GUI.Button("Loading...");
		}

		if (this.lockLoadImageButton[0] == false) {
			GUI.enabled = (bindCount > 0) ? true : false;
			if (Scene_00_GUI.Button("Load Image File 1")) {
				/* Start the loading process of the image file in the directory. */
				StartCoroutine(this.LoadImageFile("sample_image1.png", 0));
			}
		} else {
			/* Disable the GUI button during loading. */
			Scene_00_GUI.Button("Loading...");
		}

		if (this.lockLoadImageButton[1] == false) {
			GUI.enabled = (bindCount > 0) ? true : false;
			if (Scene_00_GUI.Button("Load Image File 2")) {
				/* Start the loading process of the image file in the directory. */
				StartCoroutine(this.LoadImageFile("sample_image2.png", 1));
			}
		} else {
			/* Disable the GUI button during loading. */
			Scene_00_GUI.Button("Loading...");
		}
		
		GUILayout.Space(8);
		GUI.enabled = true;
		if (Scene_00_GUI.Button("Reset")) {
			/* Destroy the texture for displaying the image file loaded. */
			for (int i = 0; i < 2; i++) {
				if (this.textures[i]) {
					this.textures[i] = null;
				}
			}
			/* Destroy the content of the loaded text. */
			this.loadedText = "";
		}

		GUILayout.EndArea();
		
		Scene_00_GUI.EndGui();
	}
	#endregion
}

/* end of file */
