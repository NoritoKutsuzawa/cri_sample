﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、はCRI File SysytemのネイティブAPIをラッパしたクラスを利用
 * しています。
 * yield命令は使用せずに、Update関数で状態を監視しています。
 * パックファイル"sample.cpk"内の、イメージファイル"criware.png"、アセット
 * バンドルファイル"CharaMw.unity3d”をロードしています。
 * 「バインダ」は、仮想的なドライブのようなもので、バインダに対してパック
 * したファイル(CPKファイル)を結びつける(BindCpk)とその内部のファイルをロ
 * ードすることができます。
 * 
 * また、URLを指定してネットワーク上のパックファイルをバインド（結びつける）
 * することもできます。
 *
 * 複数のパックファイルをバインドすることができますので、BindCpkの返り値は
 * バインドIDと呼ばれる数値です。複数のファイルをバインドすることを「マル
 * チバインド」といいます。
 * 「バインダ＋ファイル名」をローダーに指定することによって、ファイルをロー
 * ドすることができます。
 * 
 * (1) バインダCriFsBinderに"sample.cpk"をバインド。 (BindCpk関数)
 * (2) バインド完了をUpdate関数内でによりチェック。 (GetStatus関数)
 * (3) バインダとファイル名("criware.png", "CharMw.unity3d")を指定して、
 *     ローダーに読み込み開始を実行。 (Load関数)
 * (4) Update関数内でローダーの読み込み完了をチェック。(GetStatus関数)
 * (5) アセットバンドルの場合は、作成の完了をチェックした後、Instatiate関数
 *     によって実態化。
 * (6) ロード中にストップするときは、ローダーに対してStop関数を実行。
 *
 * <Tips>
 *   - 複数のファイルを読み込むときは、複数のローダーを使用して同時に読み
 *     込みリクエストを行った方が高速に読み込む事ができます。
 *   - CPKファイルをバインドすることによって、高速にファイルサイズの取得
 *     ができます。パックしていないファイルのファイルサイズを取得すると処理
 *     がブロッキングされてしまいます。
 */
/*EN
 *  This sample uses a class that wraps the native API of the CRI File System 
 *  (FileMajik PRO runtime library). 
 *
 *  For details about this sample, please see the CRIWARE Unity Plug-in Manual
 *    "Samples"->"Samples for FileMajik PRO"->"[CriFs] Native API wrapper"
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class Scene_06_NativeApi : MonoBehaviour 
{
	#region Variables
	/*	URL address for the pack file	*/
	private const string RemoteUrl = "http://crimot.com/sdk/sampledata/crifilesystem/";
	/*	pack file name	*/
	private const string CpkFilename = "sample.cpk";
	/*	image file name	*/
	private const string ImageFilename = "criware.png";	
	/*	text file name	*/
	private const string TextFilename = "sample_text.txt";
	/*	file name of Assets bundle	*/

	/*	binder	*/
	private CriFsBinder binder = null;
	/*	binder ID	*/
	private uint bind_id = 0;
	/*	Which pack file is bound? (0:no-binding, 1:local, 2:remote)	*/
	private int cpk_no = 0;
	/*	loader	*/
	private CriFsLoader loader1 = null;	/*	for image data		*/
	private CriFsLoader loader2 = null;	/*	for Assets bundle	*/
	/*	read buffer	*/
	private byte[] buffer1=null;		/*	for image data 	*/
	private byte[] buffer2=null;		/*	for text data 	*/

	/* storage for the loaded data */
	private Texture2D texture;
	private string loadedText;

	/*	binding status	*/
	private enum BindStatus
	{
		Unbind,				/*	being not bound	*/
		Binding,			/*	in progress		*/
		Complete,			/*	being bound		*/
	};
	private BindStatus bind_stat = BindStatus.Unbind;
	#endregion

	#region Function	

	/* Executed at the creation of instance. */
	void Awake()
	{
		binder  = new CriFsBinder();
		loader1 = new CriFsLoader();
		loader2 = new CriFsLoader();
	}
	
	/*	Execute the processing every frame.	*/
	void Update() 
	{
		/*	The binding is in progress?	 */
		if ( bind_stat == BindStatus.Binding ) {	/*	Check the completion of binding.	*/
			if ( CriFsBinder.GetStatus(bind_id) == CriFsBinder.Status.Complete ) {
				bind_stat = BindStatus.Complete;	/*	Change the status to completed.	*/
			}
		}
		/*	The image file loading is in progress?	 */
		if ( loader1.GetStatus() == CriFsLoader.Status.Complete ) {
			this.texture = new Texture2D(0, 0);
			this.texture.LoadImage(buffer1);		/*	The image data is in buffer1.	*/
			loader1.Stop();
		}
		/*	The text file loading is in progress?	 */
		if ( loader2.GetStatus() == CriFsLoader.Status.Complete ) {
			/* If successful, convert the byte string to text data. */
			using (var st = new StreamReader(new MemoryStream(buffer2), Encoding.UTF8)) {
				this.loadedText = st.ReadToEnd();
			}
			loader2.Stop();
		}
	}
	/*	The GUI control	*/
	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");
		
		/* Set UI skin */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		/*	Display textures.	*/
		if (this.texture) {
			GUI.DrawTexture(new Rect(0, 250, this.texture.width, this.texture.height), this.texture);
		}
		GUI.Label(new Rect(10, 80, Screen.width, 200),  this.loadedText);
		
		GUILayout.BeginArea(new Rect(Screen.width - 416, 60 , 400, 400));
		
		GUILayout.Label("Step1 : Bind CPK File");
		
		/*  Bind a pack file (sample.cpk) on the local storage. */
		if (Scene_00_GUI.Button(((this.cpk_no == 1) ? "*" : " ") + " Bind CPK File (Local)")) {
			if ( bind_stat == BindStatus.Unbind ) {		/*	Make sure that it is not bound.	*/
				/*	Make a path for a local pack file.	*/
				string path = Path.Combine(CriWare.streamingAssetsPath, CpkFilename);
				/*	Execute the binding.	*/
				bind_id = binder.BindCpk(null, path);
				/*	Change the status to that binding is in progress.	*/
				bind_stat = BindStatus.Binding;
				cpk_no = 1;		/*	a local file	*/
			}
		}
		if (Scene_00_SampleListData.supportNetworkAccess) {
			/*  Bind a pack file (sample.cpk) on the network. */
			if (Scene_00_GUI.Button(((this.cpk_no == 2) ? "*" : " ") + " Bind CPK File (URL)")) {
				if ( bind_stat == BindStatus.Unbind ) {
					/*	Make a URL path for a pack file on the network.	*/
					string path = Path.Combine(RemoteUrl, CpkFilename);
					/*	Execute the binding.	*/
					bind_id = binder.BindCpk(null, path);
					/*	Change the status to that binding is in progress.	*/
					bind_stat = BindStatus.Binding;
					cpk_no = 2;		/*	a file on the network	*/
				}
			}
		}
		GUILayout.Space(8);
		GUILayout.Label("Step2 : Load File");
	
		/*	Check whether the binder is bound and the loader status is stop.	*/
		if (bind_stat == BindStatus.Complete && loader1.GetStatus() == CriFsLoader.Status.Stop ) {
			GUI.enabled = true;
		} else {
			GUI.enabled = false;
		}
		
		/*	Load an image file (criware.png).	*/
		if (Scene_00_GUI.Button("Load Image File")) {
			/*	image file name	*/
			string path = ImageFilename;
			/*	Get the file size.	*/
			int file_size = (int)binder.GetFileSize(path);
			/*	Allocate the read buffer.	*/
			buffer1  = new byte[file_size];
			/*	Issue a request for loading. (the third argument is the offset in the file.)	*/
			loader1.Load(binder, path, 0, file_size, buffer1);
		}

		/*	Check whether the binder is bound and the loader status is stop.	*/
		if (bind_stat == BindStatus.Complete && loader2.GetStatus() == CriFsLoader.Status.Stop) {
			GUI.enabled = true;
		} else {
			GUI.enabled = false;
		}

		/*	Load a text file	*/
		if (Scene_00_GUI.Button("Load Text File")) {
			/*	text file name	*/
			string path = TextFilename;
			/*	Get the file size.	*/
			int file_size = (int)binder.GetFileSize(path);
			/*	Allocate the read buffer.	*/
			buffer2  = new byte[file_size];
			/*	Issue a request for loading. (the third argument is the offset in the file.)	*/
			loader2.Load(binder, path, 0, file_size, buffer2);
		}

		GUI.enabled = true;
		
		/*	Stop the data loading.	*/
		if (Scene_00_GUI.Button("Stop Loading")) {
			this.loader1.Stop();
			this.loader2.Stop();
		}
		
		GUILayout.Space(8);
		GUI.enabled = true;
		/*	Reset the binder and the loaded data.	*/
		if (Scene_00_GUI.Button("Reset")) {
			this.ClearAll();
		}

		GUILayout.EndArea();
		
		Scene_00_GUI.EndGui();
	}
	/*	Release the binder and erase the data.	*/
	private void ClearAll()
	{
		/* Destroy the content of the loaded text. */
		this.loadedText = "";
		
		if (this.texture) {
			this.texture = null;
		}

		if ( this.bind_stat == BindStatus.Complete ) {
			/*	Release the bound CPK file.	*/
			CriFsBinder.Unbind(this.bind_id);
			this.cpk_no = 0;
			this.bind_stat = BindStatus.Unbind;
			this.bind_id = 0;
		}
	}
	
	/*	The processing when the scene is destroyed.	*/
	void OnDisable()
	{
		ClearAll();
		/*	Dispose the loaders.	*/
		this.loader1.Dispose();
		this.loader2.Dispose();
		/*	Dispose the binders.	*/
		this.binder.Dispose();
	}
	#endregion
}

/* end of file */
