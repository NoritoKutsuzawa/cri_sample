﻿/****************************************************************************
 *
 * Copyright (c) 2016 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;
using System.Collections;

public class Scene_07_WebInstall : MonoBehaviour
{
	private CriFsWebInstaller webInstaller;
	private string url = "https://sdk.criware.com/support_e/sample/fs_sample_data/data00.dat";
	private string destFileName = "data00.dat";

	void Start ()
	{
		// Initialize CriFsWebInstaller module
		CriFsWebInstaller.InitializeModule(CriFsWebInstaller.defaultModuleConfig);

		// Create CriFsWebInstaller
		webInstaller = new CriFsWebInstaller();
	}

	void OnDestroy()
	{
		// Destroy CriFsWebInstaller
		webInstaller.Dispose();
		webInstaller = null;

		// Finalize CriFsWebInstaller module
		CriFsWebInstaller.FinalizeModule();
	}

	void Update ()
	{
		CriFsWebInstaller.ExecuteMain();
	}

	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}

		Scene_00_GUI.BeginGui("01/SampleMain");

		// Set UI skin
		GUI.skin = Scene_00_SampleList.uiSkin;

		GUILayout.BeginArea(new Rect(40, 100 , 800, 400));

		// Display CriFsWebInstaller Status
		var statusInfo = webInstaller.GetStatusInfo();
		string statusString =
			"Status: " + statusInfo.status.ToString() + "\n" +
			"Error: " + statusInfo.error.ToString() + "\n" +
			"HTTP Status Code: " + statusInfo.httpStatusCode.ToString() + "\n" +
			"Progress: " + (float)statusInfo.receivedSize / (statusInfo.contentsSize == -1 ? 1 : statusInfo.contentsSize) * 100 + " %" + "\n" +
			"Recived Size: " + statusInfo.receivedSize.ToString() + " / " + statusInfo.contentsSize.ToString() + "\n";
		GUILayout.Label(statusString);

		GUILayout.Space(8);
		GUI.enabled = (statusInfo.status == CriFsWebInstaller.Status.Stop);
		if (Scene_00_GUI.Button("Start Install", GUILayout.Width(400))) {
			string destPath = System.IO.Path.Combine(CriWare.installTargetPath, destFileName);
			if (System.IO.File.Exists(destPath))
			{
				Debug.LogWarning("Even if install target file already exists, CriWare doesn't delete or overwrite it.");
				System.IO.File.Delete(destPath);
	        }
			// Start Install
			webInstaller.Copy(this.url, destPath);
		}
		GUI.enabled = (statusInfo.status != CriFsWebInstaller.Status.Stop);
		if (Scene_00_GUI.Button("Stop", GUILayout.Width(400))) {
			// Stop CriFsWebInstaller
			webInstaller.Stop();
		}
		GUI.enabled = true;

		GUILayout.EndArea();

		Scene_00_GUI.EndGui();
	}
}
