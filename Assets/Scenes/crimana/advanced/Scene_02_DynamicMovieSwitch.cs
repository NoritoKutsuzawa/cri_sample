﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、ムービ再生の動的な切り替えを行います。
 *
 * 複数のCriManaPlayerコンポーネントを使用してつなぎ目なく
 * すぐに再生中の別のムービを再生することが可能です。
 */
/*EN
 * This sample performs the dynamic switching of movie playbacks.
 *
 * By using multiple CriManaPlayer components, seamless and instant 
 * switching to another movie is possible.
 */

using UnityEngine;
using System.Collections;

public class Scene_02_DynamicMovieSwitch : MonoBehaviour
{
	/* Material to use as model for movie players. */
	public Material movieMaterial = null;
	/* Target meshes for the movie materials */
	public MeshRenderer[] targetMeshes = null;

	/* Currently playing movie player index. */
	private int curIndex = 0;
	/* A list of players. */
	private CriManaMovieMaterial[] moviePlayers = new CriManaMovieMaterial[2];
	/* A list of movies files. */
	private string[] moviePathes =  {
		"movie.usm",
		"criware_movie.usm",
	};

	/* Start */
	void Start()
	{
		/* Prepare all the player handles. */
		for (int i = 0; i < moviePlayers.Length; i++) {
			moviePlayers[i] = this.gameObject.AddComponent<CriManaMovieMaterial>();
			moviePlayers[i].player.SetFile(null, moviePathes[i]);

			/* Set materials. */
			moviePlayers[i].material = new Material(movieMaterial);
			moviePlayers[i].loop = true;

			if (i == 0) {
				/* Start player #0 only. */
				moviePlayers[0].player.Start();
				foreach (var mesh in targetMeshes)
				{
					mesh.material = moviePlayers[0].material;
				}
			} else {
				/* Prepare other players for playback. */
				moviePlayers[i].player.Prepare();
			}
		}
	}

	void Update()
	{
		/* Search all the players. */
		for (int i = 0; i < moviePlayers.Length; i++) {
			/* Separete the processing by the player status. */
			switch (moviePlayers[i].player.status) {
				case CriMana.Player.Status.Stop:
					/* Return to the preparation status after the playback is finished. */
					moviePlayers[i].player.Prepare();
					break;
				default:
					break;
			}
		}
	}

	/* Displaying GUI and prosessing user operations */
	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}

		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;


		GUILayout.BeginArea(new Rect(36, 82, 320, 250), Scene_00_SampleList.TextStyle);
		{
			{
				int nextIndex = curIndex;
				if (Scene_00_GUI.Button("Player 1")) {
					nextIndex = 0;
				}
				if (Scene_00_GUI.Button("Player 2")) {
					nextIndex = 1;
				}
				if (nextIndex != curIndex) {
					/* A processing when a movie is selected by user operations. */
					/* - Starts the next movie.
					 * - Wait for 2 frames: 
					 * - 1 frame to let player goes frome ready status to playing status.
					 * - 1 frame to be sure a valid texture was obtained from CriWare native render plugin. (may be optional)
					 * - Stops the current movie and switches materials on target meshes. */
					StartCoroutine(SwitchMovies(nextIndex, curIndex));
					curIndex = nextIndex;
				}
			}
			GUILayout.Space(20);
			GUILayout.Label("<Player Information>");
			for (int i = 0; i < moviePlayers.Length; i++) {
				GUILayout.Label("Player" + (i + 1) + " Status : " + moviePlayers[i].player.status);
			}
		}
		GUILayout.EndArea();

		Scene_00_GUI.EndGui();
	}

	private IEnumerator SwitchMovies(int nextIndex, int curIndex)
	{
		/* Start to play next movie */
		moviePlayers[nextIndex].Play();

		/* Wait some frames to let native plugin ready send to new texture to material. */
		/* - Jump after update of current frame (no frame lost) (request playing). */
		yield return null;
		/* - Jump to next frame (playing status -> request texture to native plugin). */
		yield return null;
		/* - Depending platform and it may be needed to wait one more frame. (When external textures are used to avoid extra texture copies or when h.264 is used.) */
		/*   Jump to the 2nd next frame (texture ready to be displayed). */
		yield return null;
		
		/* Stops current movie. */
		moviePlayers[curIndex].Stop();

		/* Sets playing movie material on target meshes. */
		foreach (var mesh in targetMeshes) {
			mesh.material = moviePlayers[nextIndex].material;
		}
	}
}


