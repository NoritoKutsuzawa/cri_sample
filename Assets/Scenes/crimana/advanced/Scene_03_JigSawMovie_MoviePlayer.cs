/****************************************************************************
 *
 * Copyright (c) 2013 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;
using System.Collections;

public class Scene_03_JigSawMovie_MoviePlayer : MonoBehaviour {
	public CriManaMovieMaterial movieMaterial;
	private Material material;

	// Use this for initialization
	void Awake() {
		material = new Material(Shader.Find("VertexLit"));
		movieMaterial.material = material;
	}

	void OnDestroy() {
		Destroy (material);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
