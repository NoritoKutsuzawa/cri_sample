/****************************************************************************
 *
 * Copyright (c) 2013 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;
using System.Collections;

public class Scene_03_JigSawMovie_Quad : MonoBehaviour {
	public CriManaMovieMaterial movieMaterial;

	// Use this for initialization
	void Start() {
		Renderer renderer = GetComponent<Renderer>();
		renderer.material = movieMaterial.material;
	}

	// Update is called once per frame
	void Update() {

	}
}
