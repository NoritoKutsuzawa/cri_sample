﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、CRI File SystemのユーティリティAPIを使い、
 * ローカル上のCPKファイルをバインドした後、
 * CPKファイル内のムービーファイルを再生します。
 * 単体ファイルを読み込む際は、CPKをバインドしたバインダハンドルを参照し、
 * CPK内でのファイル名でアクセスします。
 */
/*EN
 * This sample plays a movie file in the CPK file on the local storage,
 * after binding it by the utility API of CRI File System.
 * To load a single file, access the file in the CPK file 
 * by referencing the binder handle which is bound to the CPK file.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Scene_03_PlaybackCpk : MonoBehaviour
{
	/* an object to perform "AddComponent" for CriManaPlayer */
	public  GameObject movieObject = null;

	/* an object to reference CriManaPlayer in which "AddComponent" is applied to movieObject */
	private CriManaMovieController moviePlayer = null;

	#region Variables
	/* path name for local CPK file (to be created at runtime) */
	private string localCpkFilePath;

	/* file name for loading from CPK */
	private const string movieFilename = "sample_256x256.usm";

	/* binder handle */
	private CriFsBinder binder = null;
	/* binder ID */
	private uint bindId = 0;

	/* lock flag for GUI button */
	private bool lockBindCpkButton = false;
	#endregion

	#region Functions
	/* Initialization for the scene */
	void OnEnable()
	{
		/* Create a binder. */
        /*JP
		 * ファイルバインド、CPKバインド、ディレクトリバインド等のバインド機能を使う場合、
		 * アプリケーションが明示的にバインダを作成する必要があります。
         */
        /*EN
		 * To use the binding feature such as file binding, CPK binding and directory binding,
		 * the application needs to explicitly create a binder.
		 */
		this.binder = new CriFsBinder();

		/* Create a file path for the local CPK file. */
		this.localCpkFilePath = Path.Combine(CriWare.streamingAssetsPath, "movie_sample.cpk");
	}

	/* Finalization for the scene */
	void OnDisable()
	{
		/* Reset the scene. */
		this.reset();

		/* Execute the unbind processing. */
		this.UnbindCpk();

		/* Destroy the binder */
		/* The application needs to explicitly destroy the used binder. */
		this.binder.Dispose();
		this.binder = null;
	}

	/* CPK binding process */
	/*JP
	 * 本サンプルではコルーチン内でCPKのバインドの完了を待っていますが、
	 * Updateメソッド内でバインドの完了をチェックする方法もあります。
	 * 詳しくは CriFileSystem のサンプルを参照してください。
	 */
	/*EN
	 * This sample waits for the completion of CPK binding in the coroutine.
	 * Another way that checks for the completion of binding in the Update method
	 * is also available. For details, please see samples for CRI File System.
	 */
	private IEnumerator BindCpk(string path, int cursor)
	{
		/* Release the bound CPK. */
		this.UnbindCpk();

		this.lockBindCpkButton = true;

		/* Request a CPK binding. */
		/*
		 * The content of the CPK file is bound to the specified binder handle.
		 * Declaring request as var and resolving by type inference is OK.
		 */
		CriFsBindRequest request = CriFsUtility.BindCpk(this.binder, path);

		/* Store the binding ID in the application. */
		/* Binder ID is an ID to uniquely identify the file binding, */
		/* which is used for the release processing etc. */
		this.bindId = request.bindId;
		
		/* Wait until binding is complete. */
		yield return request.WaitForDone(this);
		this.lockBindCpkButton = false;

		if (request.error == null) {
			Debug.Log("Completed to bind CPK. (path=" + path + ")");
		} else {
			/* If failed, execute Unbind process. */
			this.UnbindCpk();
			Debug.LogWarning("Failed to bind CPK. (path=" + path + ")");
		}
	}

	/* Release processig of the bound CPK. */
	private void UnbindCpk()
	{
		if (this.bindId > 0) {
			/* Release the bound CPK. */
			CriFsBinder.Unbind(this.bindId);
			this.bindId = 0;
		}
	}

	/* Reset the scene. */
	private void reset()
	{
		/* Stop the movie playback. */
		if (moviePlayer != null) {
			moviePlayer.Stop();
			Destroy(moviePlayer);
		}
		/* The release processing of CPK. */
		this.UnbindCpk();
	}

	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		GUILayout.BeginArea(new Rect(Screen.width - 466, 70 , 450, 32* 4));
		
		if (this.lockBindCpkButton == false) {
			if (Scene_00_GUI.Button(((this.bindId > 0) ? "*" : " ") + " Bind CPK File")) {
				/* Start the binding process of local CPK file. */
				StartCoroutine(this.BindCpk(this.localCpkFilePath, 1));
			}
		} else {
			Scene_00_GUI.Button("Binding...");
		}

		if (this.bindId > 0) {
			/*JP
			 * 本サンプルではムービーの再生開始時に CriManaPlayer を AddComponent して、
			 * ムービーの再生終了時か Stop ボタンが押された際に Destroy します。
			 */
			/*EN
			 * This sample performs the AddComponent of CriManaPlayer when a movie playback is started.
			 * If the playback ends or the Stop button is clicked, the object is destroyed.
			 */
			if (moviePlayer == null) {
				/* The Play button processing. */
				if (Scene_00_GUI.Button("Play")) {
						/* Perform the AddComponent of CriManaPlayer to movieObject. */
						moviePlayer = movieObject.AddComponent<CriManaMovieController>();
						moviePlayer.useOriginalMaterial = true;
						/* Set the binder and the file name. */
						moviePlayer.player.SetFile(binder, movieFilename);
						/* Start a playback. */
						moviePlayer.player.Start();
					}
			}
			else {
				switch (this.moviePlayer.player.status) {
				case CriMana.Player.Status.Playing:
					/* The Stop button processing. */
					if (Scene_00_GUI.Button("Stop")) {
						moviePlayer.player.Stop();
					}
				break;
				case CriMana.Player.Status.PlayEnd:
					/* Call the Stop() even when the playback is finished. */
					moviePlayer.player.Stop();
				break;
				case CriMana.Player.Status.Stop:
					/* Destroy the CriManaMovieController. */
					Destroy(moviePlayer);
				break;
				}
			}
		} else {
			GUI.enabled = false;
			Scene_00_GUI.Button("Play");
		}
		GUI.enabled = true;

		GUILayout.Space(8);

		/* Reset the scene. */
		if (Scene_00_GUI.Button("Reset")) {
			this.reset();
		}
		
		GUILayout.EndArea();
		
		Scene_00_GUI.EndGui();
	}
	#endregion
}

/* end of file */
