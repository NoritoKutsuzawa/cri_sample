﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、ネットワークからインストールしたCPKファイル
 *（ファイルマジックPROのパックファイル）からムービ再生を行います。
 */
/*EN
 * This sample plays a movie from a CPK file (a pack file from FileMajik PRO) 
 * which is installed from the network. 
 */

using UnityEngine;
using System.Collections;
using System.IO;

public class Scene_04_DownloadPlayback : MonoBehaviour 
{
	#region Variables
	/* an object to perform "AddComponent" for CriManaPlayer */
	public  GameObject movieObject = null;

	/* an object to reference CriManaPlayer in which "AddComponent" is applied to movieObject */
	private CriManaMovieController movieController = null;

	/* source directory to load files */
	/* It is defined to load AssetBundle files for each platform. */
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
	const string AssetDir = "WIN_OSX/";
#elif UNITY_IPHONE
	const string AssetDir = "iOS/";
#elif UNITY_ANDROID
	const string AssetDir = "Android/";
#endif
	
	private const string RemoteUrl = "http://crimot.com/sdk/sampledata/crimana/";
	private const string CpkFilename = "sample.cpk";
	private const string AssetFilename = "CharaMw.unity3d";
	
	private string localCpkPath;
	private string remoteCpkPath;
	
	/* file name for loading from CPK file */
	private const string movieFilename = "sample_256x256.usm";
	
	/* storage area for the install request */
	private CriFsInstallRequest[] installRequest = new CriFsInstallRequest[2];
	
	/* parameters for CPK binding */
	private CriFsBinder binder = null;
	private uint bindId = 0;

	/* lock flag for GUI button */
	private bool lockInstallButton = false;
	private bool lockBindCpkButton = false;
	private bool showBindCpkButton = false;
	#endregion

	#region Functions
	/* Initialization for the scene */
	void OnEnable()
	{
		/* Create a binder. */
        /*JP
		 * ファイルバインド、CPKバインド、ディレクトリバインド等のバインド機能を使う場合、
		 * アプリケーションが明示的にバインダを作成する必要があります。
         */
        /*EN
		 * To use the binding feature such as file binding, CPK binding and directory binding,
		 * the application needs to explicitly create a binder.
		 */
		this.binder = new CriFsBinder();

		/* Create paths. */
		/* Create the path of the installation destination. */
		this.localCpkPath = Path.Combine(CriWare.installTargetPath, CpkFilename);

		/* Create the path of the installation source. */
		this.remoteCpkPath = Path.Combine(RemoteUrl, CpkFilename);
	}

	/* Finalization for the scene */
	void OnDisable()
	{
		/* Reset the scene. */
		this.reset();

		/* Execute the unbind processing. */
		this.UnbindCpk();

		/* Destroy the binder. */
		/* The application needs to explicitly destroy the used binder. */
		this.binder.Dispose();
		this.binder = null;
	}
	
	private IEnumerator Install(string src_path, string install_path, int request_index)
	{
		this.showBindCpkButton = false;
		/* Request the installation. */
		this.installRequest[request_index] = CriFsUtility.Install(null, src_path, install_path);
		/* Wait until the installation is complete. */
		yield return this.installRequest[request_index].WaitForDone(this);
		/* 
		 * [Note]
		 * The request for the installation is stored in the application,
		 * and it is used for a flag to prevent multiple installations from occurring.
		 */
		
		if (this.installRequest[request_index].error == null) {
			Debug.Log("Completed installation." + src_path);
			this.showBindCpkButton = true;
		} else {
			Debug.LogWarning("Failed installation." + src_path);
		}
		/* Release the request after the installation is complete. */
		this.installRequest[request_index] = null;
	}
	
	/* CPK binding process */
	/*JP
	 * 本サンプルではコルーチン内でCPKのバインドの完了を待っていますが、
	 * Updateメソッド内でバインドの完了をチェックする方法もあります。
	 * 詳しくは CriFileSystem のサンプルを参照してください。
	 */
	/*EN
	 * This sample waits for completion of CPK binding in the coroutine.
	 * Another way that checks for completion of binding in the Update method
	 * is also available. For details, please see samples for CRI File System.
	 */
	private IEnumerator BindCpk(string path, int cursor)
	{
		/* Release the bound CPK. */
		this.UnbindCpk();

		this.lockBindCpkButton = true;

		/* Request a CPK binding. */
		/*
		 * The content of the CPK file is bound to the specified binder handle.
		 * Declaring request as var and resolving by type inference is OK.
		 */
		CriFsBindRequest request = CriFsUtility.BindCpk(this.binder, path);

		/* Store the binding ID in the application. */
		/* Binder ID is an ID to uniquely identify the file binding, */
		/* which is used for the release process etc. */
		this.bindId = request.bindId;

		/* Wait until binding is complete. */
		yield return request.WaitForDone(this);
		this.lockBindCpkButton = false;

		if (request.error == null) {
			Debug.Log("Completed to bind CPK. (path=" + path + ")");
		} else {
			/* If failed, execute Unbind process. */
			this.UnbindCpk();
			Debug.LogWarning("Failed to bind CPK. (path=" + path + ")");
		}
	}

	/* Release processing of the bound CPK */
	private void UnbindCpk()
	{
		if (this.bindId > 0) {
			/* Release the bound CPK. */
			CriFsBinder.Unbind(this.bindId);
			this.bindId = 0;
		}
	}

	/* Reset the scene. */
	private void reset()
	{
		/* Stop the movie playback. */
		if (movieController != null) {
			movieController.player.Stop();
			Destroy(movieController);
		}
		/* Release the CPK. */
		this.UnbindCpk();
		
		lockInstallButton = false;
		lockBindCpkButton = false;
		showBindCpkButton = false;
	}

	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		GUILayout.BeginArea(new Rect(Screen.width - 416, 70 , 400, 32 * 6));
		
		if (this.showBindCpkButton == false) {
			if (this.installRequest[0] == null) {
				if (this.lockInstallButton == false) {
					if (Scene_00_GUI.Button("Install CPK File")) {
						/* Unbind the CPK before installation because the CPK file is overwritten. */
						this.UnbindCpk();
						/* Start the installation process for CPK file. */
						StartCoroutine(this.Install(this.remoteCpkPath, this.localCpkPath, 0));
					}
				}
			} else {
				/* Get the progress from the installation request and display it on the screen. */
				float progress = this.installRequest[0].progress;
				GUILayout.Label("Installing... " + (int)(progress * 100) + "%");
				GUILayout.Box("", GUILayout.Width(400 * progress + 10), GUILayout.Height(8));
			}
		}
		
		if (this.showBindCpkButton == true) {
			if (this.lockBindCpkButton == false) {
				GUILayout.Label("Loaded CPK File");
				if (Scene_00_GUI.Button(((this.bindId > 0) ? "*" : " ") + " Bind CPK File")) {
					/* Start the binding process of local CPK file. */
					StartCoroutine(this.BindCpk(this.localCpkPath, 1));
				}
				if (this.bindId <= 0) {
					GUI.enabled = false;
					Scene_00_GUI.Button("Play");
				}
				GUI.enabled = true;

			} else {
				Scene_00_GUI.Button("Binding...");
			}
		}

		if (this.bindId > 0 && this.lockBindCpkButton == false) {
			/*JP
			 * 本サンプルではムービーの再生開始時に CriManaPlayer を AddComponent して、
			 * ムービーの再生終了時か Stop ボタンが押された際に Destroy します。
			 */
			/*EN
			 * This sample performs the AddComponent of CriManaPlayer when a movie playback is started.
			 * If the playback ends or the Stop button is clicked, the object is destroyed.
			 */
			if (movieController == null) {
				/* The Play button processing. */
				if (Scene_00_GUI.Button("Play")) {
					this.lockInstallButton = true;
					/* Perform the AddComponent of CriManaPlayer to movieObject. */
					movieController = movieObject.AddComponent<CriManaMovieController>();
					movieController.useOriginalMaterial = true;
					/* Set the binder and the file name. */
					movieController.player.SetFile(binder, movieFilename);
					/* Start a playback. */
					movieController.player.Start();
				} 
			}
			else {
				switch (movieController.player.status) {
				case CriMana.Player.Status.Playing:
					/* The Stop button processing. */
					if (Scene_00_GUI.Button("Stop")) {
						movieController.player.Stop();
					}
				break;
				case CriMana.Player.Status.PlayEnd:
					/* Call the Stop() even when the playback is finished. */
					movieController.player.Stop();
				break;
				case CriMana.Player.Status.Stop:
					/* Destroy the CriManaPlayer. */
					Destroy(movieController);
					this.lockInstallButton = false;
				break;
				}
			}
		}

		GUILayout.Space(8);

		/* Reset the scene. */
		if (Scene_00_GUI.Button("Reset")) {
			this.reset();
		}
		
		GUILayout.EndArea();
		
		Scene_00_GUI.EndGui();
	}
	#endregion
}
