﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

/*JP
 * 本サンプルは、スライダーで指定された位置からのシーク再生を行います。
 */
/*EN
 * This sample performs seek playback from any position specified by the slider.
 */

using UnityEngine;

public class Scene_05_Seek : MonoBehaviour {
	/* an object to reference CriManaPlayer in which "AddComponent" is applied to movieObject */
	public CriManaMovieController movieController = null;

	private int  seekFrameNumber = 0;
	private uint totalFrames = 0;
	private bool requestedSeek = false;

	void OnGUI()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");
		
		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		Rect playbackBarRect = new Rect(32, Screen.height - 80, Screen.width - 64,  32);
		Rect seekBarRect     = new Rect(32, Screen.height - 64, Screen.width - 64,  32);
        Rect seekButtonRect  = new Rect(32, Screen.height - 48, Screen.width - 64,  32);

        if ( ( (movieController.player.status == CriMana.Player.Status.Playing) || 
		       (movieController.player.status == CriMana.Player.Status.PlayEnd) ) ) {

			totalFrames = movieController.player.movieInfo.totalFrames;
			GUI.enabled = false;
			int frameNo = movieController.player.frameInfo != null ? movieController.player.frameInfo.frameNo : 0;
			Scene_00_GUI.HorizontalSlider(playbackBarRect, frameNo, 0, totalFrames);
			GUI.enabled = true;
			seekFrameNumber = (int)Scene_00_GUI.HorizontalSlider(seekBarRect, seekFrameNumber, 0, totalFrames);
            var oldFrameNum = seekFrameNumber;
            seekFrameNumber = (int)Scene_00_GUI.HorizontalSliderButton(seekButtonRect, seekFrameNumber, 0, totalFrames, totalFrames/20);
            if ((GUI.changed)||(seekFrameNumber != oldFrameNum)) {
				/* Stop the playback. (Seeking is not available during playback.)  */
				movieController.player.Stop();
				requestedSeek = true;
			}
		} else {
			GUI.enabled = false;
			Scene_00_GUI.HorizontalSlider(playbackBarRect, 0, 0, totalFrames);
			GUI.enabled = true;
			Scene_00_GUI.HorizontalSlider(seekBarRect, seekFrameNumber, 0, totalFrames);
		}
		
		Scene_00_GUI.EndGui();
	}

	void Update() {

		if (requestedSeek && movieController.player.status == CriMana.Player.Status.Stop) {
			/* Seek the movie and start the playback.  */
			movieController.player.SetSeekPosition(seekFrameNumber);
			movieController.player.Start();
			requestedSeek = false;
		}
	}
}
