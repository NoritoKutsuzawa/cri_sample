﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;
using System.Collections.Generic;

public class Scene_06_SeamlessSequencePlayback : MonoBehaviour {
	/* an object to reference CriManaPlayer in which "AddComponent" is applied to movieObject */
	public CriManaMovieController movieController = null;
	
	private const int numOfDisplay = 10;
	private Vector2 displayPosition = new Vector3(0.0f, 100.0f);
	
	/* movie file path */
	private readonly string[] contentsList = {
		"Seamless/seamless_sample_1.usm",
		"Seamless/seamless_sample_2.usm",
		"Seamless/seamless_sample_3.usm",
		"Seamless/seamless_sample_4.usm",
		"Seamless/seamless_sample_5.usm",
		"Seamless/seamless_sample_6.usm",
		"Seamless/seamless_sample_7.usm",
	};
	private List<int>         entryList       = new List<int>();
	private List<int>         loadedEntryList = new List<int>();
	private int               currentIdx      = -1;
	

	void Update() {
		if ((entryList.Count > 0) && (currentIdx >= 0)) {
			if (movieController != null) {
				int currentCntConcat = (movieController.player.status == CriMana.Player.Status.Playing)
					&& (movieController.player.frameInfo != null)
					? (int)movieController.player.frameInfo.cntConcatenatedMovie
						: 0;
				currentIdx = entryList[currentCntConcat];
			}
		}
	}
	
	
	void OnGUI() {
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}
		
		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		GUILayout.BeginArea(new Rect(displayPosition.x, displayPosition.y, 640, 480));
		{
			CriMana.Player.Status player_status = movieController.player.status;
			
			/* The Playback control button. */
			GUILayout.BeginHorizontal();
			{
				GUI.enabled = (currentIdx >= 0) && ((player_status == CriMana.Player.Status.Stop) || (player_status == CriMana.Player.Status.Ready));
				if (Scene_00_GUI.Button("Play", GUILayout.MinHeight(40), GUILayout.MinWidth(60))) {
					movieController.player.Start();
				}
				GUI.enabled = (player_status == CriMana.Player.Status.Playing);
				if (Scene_00_GUI.Button("Pause", GUILayout.MinHeight(40), GUILayout.MinWidth(60))) {
					movieController.player.Pause(!movieController.player.IsPaused());
				}
				GUI.enabled = true;
				if (Scene_00_GUI.Button("Stop", GUILayout.MinHeight(40), GUILayout.MinWidth(60))) {
					movieController.player.Stop();
					entryList.Clear();
					loadedEntryList.Clear();
					currentIdx = -1;
				}
			}
			GUILayout.EndHorizontal();
			
			/* The Movie selection button. */
			GUILayout.BeginHorizontal();
			{
				GUI.enabled = (player_status != CriMana.Player.Status.PlayEnd);
				for (int i = 0; i < contentsList.Length ; i++) {
					string button_string = (i == currentIdx) ? ("<" + (i + 1).ToString() + ">") : (i + 1).ToString();
					if(Scene_00_GUI.Button(button_string, GUILayout.MinHeight(40), GUILayout.MinWidth(40))) {
						/*JP 先頭のムービ登録は CriManaPlayer.SetMode.New でセットします。 */
						/*EN Register the first movie with the CriManaPlayer.SetMode.New.  */
						if (entryList.Count == 0) {
							movieController.player.SetFile(null, contentsList[i], CriMana.Player.SetMode.New);
							currentIdx = i;
							entryList.Add(i);
						}
						/*JP
						 * 連結するムービは CriManaPlayer.SetMode.Append でセットすることで、
						 * シームレス連結再生を行えます。
						 */
						/*EN 
						 * By setting the movies to concatenate with the CriManaPlayer.SetMode.Append, 
						 * the seamless concatenated playback can be performed.
						 */
						else {
							/* If the number of entries in the plug-in is insufficient, it returns false. */
							bool set_result = movieController.player.SetFile(null, contentsList[i], CriMana.Player.SetMode.Append);
							if (set_result) {
								entryList.Add(i);
							}
							else {
								Debug.Log("failed SetEntry (entry pool is empty).");
							}
						}
					}
				}
				GUI.enabled = true;
			}
			GUILayout.EndHorizontal();
			
			/* Display the information. */
			GUILayout.BeginVertical();
			{
				int currentCntConcat = (movieController.player.status == CriMana.Player.Status.Playing) && (movieController.player.frameInfo != null)
					? (int)movieController.player.frameInfo.cntConcatenatedMovie
					: 0;
				/* Display the concatenation status. */
				string concat_progress = "Concat Progress: ";
				for (int i = System.Math.Max(0, (entryList.Count - numOfDisplay)); i < entryList.Count; i++) {
					concat_progress +=
						(i == currentCntConcat)
						? ("<" + (entryList[i] + 1).ToString() + "> ")
						: (" " + (entryList[i] + 1).ToString() + "  ");
				}
				GUILayout.Label(concat_progress);
				/* Display the frame information. */
				int frameNo = 0;
				int frameNoPerFile = 0;
				if (movieController.player.frameInfo != null) {
					frameNo = movieController.player.frameInfo.frameNo;
					frameNoPerFile = movieController.player.frameInfo.frameNoPerFile;
				}
				GUILayout.Label(
					"Frame No: " + frameNo + "\n" +
					"Frame No Per File: " + frameNoPerFile
					);
				/* Display the concatenation count. */
				GUILayout.Label("Concat Count: " + currentCntConcat);
				/* Display the player status. */
				GUILayout.Label("Player Status: " + movieController.player.status.ToString());
			}
			GUILayout.EndVertical();
		}
		GUILayout.EndArea ();
		
		Scene_00_GUI.EndGui();
	}
}
