﻿/****************************************************************************
 *
 * Copyright (c) 2016 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using System;
using System.Collections.Generic;


class BatchWebInstaller : IDisposable
{
	#region Data Types
	public enum Status
	{
		Stop,
		Busy,
		Complete,
		ErrorOccured,
		ErrorStopped,
		StopProcessing,
	}

	public class UrlAndDestPath
	{
		public readonly string url;
		public readonly string destPath;

		public UrlAndDestPath(string url, string destPath)
		{
			this.url = url;
			this.destPath = destPath;
		}
	}

	public class InstallContext
	{
		public CriFsWebInstaller.StatusInfo statusInfo;
		public UrlAndDestPath urlAndDestPath;
	}
	#endregion

	#region Public Properties
	public Status				status { get; private set; }
	public List<UrlAndDestPath>	installQueue { get; private set; }
	public InstallContext[]		installContexts { get; private set; }
	#endregion

	#region Private Variables
	private bool						disposed	= false;
	private Queue<CriFsWebInstaller>	idleInstallers;
	private List<CriFsWebInstaller>		busyInstallers;
	private Dictionary<CriFsWebInstaller, InstallContext>	installerToContext;
	#endregion


	public BatchWebInstaller(int installConcurrency)
	{
		this.status				= Status.Stop;
		this.installQueue		= new List<UrlAndDestPath>();
		this.installContexts	= new InstallContext[installConcurrency];
		this.idleInstallers		= new Queue<CriFsWebInstaller>(installConcurrency);
		this.busyInstallers		= new List<CriFsWebInstaller>(installConcurrency);
		this.installerToContext = new Dictionary<CriFsWebInstaller, InstallContext>(installConcurrency);
		for (int i = 0; i < installConcurrency; i++) {
			var installer = new CriFsWebInstaller();
			var installContext = new InstallContext();
			installContext.statusInfo = installer.GetStatusInfo();
			this.idleInstallers.Enqueue(installer);
			this.installContexts[i] = installContext;
			this.installerToContext.Add(installer, installContext);
		}
	}

	~BatchWebInstaller()
	{
		this.Dispose(false);
	}

	public void Dispose()
	{
		this.Dispose(true);
		System.GC.SuppressFinalize(this);
	}

	public void Start()
	{
		if (this.status != Status.Stop) {
			UnityEngine.Debug.Log("[CRIWARE] TODO");
			return;
		}
		this.status = Status.Busy;
	}

	public void Stop()
	{
		switch (this.status) {
			case Status.Stop:
			case Status.StopProcessing:
				break;
			case Status.Complete:
				this.status = Status.Stop;
				break;
			case Status.Busy:
			case Status.ErrorOccured:
			case Status.ErrorStopped:
				for (int i = 0; i < this.busyInstallers.Count; i++) {
					var installer = this.busyInstallers[i];
					var context = this.installerToContext[installer];
					installer.Stop();
					this.installQueue.Insert(0, context.urlAndDestPath);
					context.urlAndDestPath = null;
				}
				this.status = Status.StopProcessing;
				break;
		}
	}

	public void Update()
	{
		switch (this.status) {
			case Status.Complete:
			case Status.ErrorStopped:
			case Status.Stop:
				break;
			case Status.Busy:
			case Status.ErrorOccured:
				CriFsWebInstaller.ExecuteMain();
				int numErrorConditionInstallers = UpdateBusyInstallers();
				if (numErrorConditionInstallers > 0) {
					if (numErrorConditionInstallers == this.busyInstallers.Count) {
						this.status = Status.ErrorStopped;
					} else {
						this.status = Status.ErrorOccured;
					}
				} else {
					// エラーが発生していない場合のみ、次のインストールを行なう
					if (this.installQueue.Count == 0) {
						if (this.busyInstallers.Count == 0) {
							this.status = Status.Complete;
						}
					} else {
						ConsumeInstallQueue();
					}
				}
				break;
			case Status.StopProcessing:
				CriFsWebInstaller.ExecuteMain();
				UpdateBusyInstallers();
				if (this.busyInstallers.Count == 0) {
					this.status = Status.Stop;
				}
				break;
			default:
				break;
		}
	}

	#region Private Methods
#if !UNITY_EDITOR && UNITY_WINRT && !ENABLE_IL2CPP
    private async void Dispose(bool disposing)
#else
    private void Dispose(bool disposing)
#endif
	{
		if (disposed) {
			return;
		}

		if (this.status != Status.Stop) {
			this.Stop();
			while (true) {
				Update();
				if (this.status == Status.Stop) {
					break;
				}
#if !UNITY_EDITOR && UNITY_WINRT && !ENABLE_IL2CPP
                await System.Threading.Tasks.Task.Delay(TimeSpan.FromSeconds(1));
#else
                System.Threading.Thread.Sleep(1);
#endif
			}
		}

		while (this.idleInstallers.Count != 0) {
			var installer = this.idleInstallers.Dequeue();
			installer.Dispose();
		}

		this.installQueue		= null;
		this.installContexts	= null;
		this.idleInstallers		= null;
		this.busyInstallers		= null;
		this.installerToContext = null;
	}

	private int UpdateBusyInstallers()
	{
		int numErrorConditionInstallers = 0;
		int i = 0;
		while (i < this.busyInstallers.Count) {
			var installer = this.busyInstallers[i];
			var context = this.installerToContext[installer];
			context.statusInfo = installer.GetStatusInfo();
			switch (context.statusInfo.status) {
				case CriFsWebInstaller.Status.Error:
					this.status = Status.ErrorOccured;
					++numErrorConditionInstallers;
					++i;
					break;
				case CriFsWebInstaller.Status.Stop:
					this.busyInstallers.RemoveAt(i);
					this.idleInstallers.Enqueue(installer);
					context.urlAndDestPath = null;
					break;
				case CriFsWebInstaller.Status.Complete:
					installer.Stop();
					this.busyInstallers.RemoveAt(i);
					this.idleInstallers.Enqueue(installer);
					context.statusInfo = installer.GetStatusInfo();
					context.urlAndDestPath = null;
					break;
				default:
					++i;
					break;
			}
		}
		return numErrorConditionInstallers;
	}

	private void ConsumeInstallQueue()
	{
		while (this.idleInstallers.Count != 0) {
			if (this.installQueue.Count == 0) {
				break;
			}
			var installer = this.idleInstallers.Dequeue();
			var urlAndDestPath = installQueue[0];
			var context = this.installerToContext[installer];
			installQueue.RemoveAt(0);
			installer.Copy(urlAndDestPath.url, urlAndDestPath.destPath);
			this.busyInstallers.Add(installer);
			context.statusInfo = installer.GetStatusInfo();
			context.urlAndDestPath = urlAndDestPath;
		}
	}
	#endregion
}