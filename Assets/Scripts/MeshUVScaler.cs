﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;
using System.Collections;

public class MeshUVScaler : MonoBehaviour {
	public Vector2 offset = new Vector2(0.0f, 0.0f);
	public Vector2 scale  = new Vector2(1.0f, 1.0f);
	
	void Start ()
	{
		/* Moving and scaling of the uv coordinates of the mesh */
		var uvs = GetComponent<MeshFilter>().mesh.uv;
		for (int i = 0; i < uvs.Length; i++) {
			uvs[i].x = (uvs[i].x * scale.x) + offset.x;
			uvs[i].y = (uvs[i].y * scale.y) + offset.y;
		}
		GetComponent<MeshFilter>().mesh.uv = uvs;
	}
}

