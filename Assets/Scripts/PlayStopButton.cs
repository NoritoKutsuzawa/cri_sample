﻿/****************************************************************************
 *
 * Copyright (c) 2018 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;

public class PlayStopButton : MonoBehaviour {

    #region Variables
    public Rect buttonRect;

    private CriManaMovieController manaController;
	private string[] buttonStrings = {"Play", "Stop"};
	#endregion

	// Use this for initialization
	void Start () {
		manaController = gameObject.GetComponent<CriManaMovieController>();
	}
	
	// Update is called once per frame
	void OnGUI ()
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}

		Scene_00_GUI.BeginGui("01/SampleMain");

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;

		CriMana.Player.Status status = manaController.player.status;
		if (status == CriMana.Player.Status.Stop || status == CriMana.Player.Status.Error) {
			if (Scene_00_GUI.Button(buttonRect, buttonStrings[0])) {
				manaController.Play();
			}
		} else {
			if (Scene_00_GUI.Button(buttonRect, buttonStrings[1])) {
				manaController.Stop();
			}
		}

		Scene_00_GUI.EndGui();
	}
}
