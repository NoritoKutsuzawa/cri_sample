﻿/****************************************************************************
 *
 * Copyright (c) 2011 CRI Middleware Co., Ltd.
 *
 ****************************************************************************/

using UnityEngine;
using System.Collections;

public class PlaybackInfoDisplay : MonoBehaviour 
{
	#region Variables
	public GameObject pastedMovieObject = null;
	
	private string timestr = "";
	private string durstr = "";
    #endregion

    float GetTime(CriMana.Player player)
    {
        if (player.frameInfo == null)
        {
            return 0;
        }
		return (float) player.frameInfo.frameNo / player.frameInfo.framerateN / player.frameInfo.framerateD * 1000000.0f;
    }

    void Update () 
	{
		this.timestr = "";
		this.durstr  = "";
		if (this.pastedMovieObject != null) {
			CriManaMovieController cri_mana_controller = pastedMovieObject.GetComponent<CriManaMovieController>();
			if (cri_mana_controller != null) {
				if (cri_mana_controller.player.status == CriMana.Player.Status.Playing && 
				    cri_mana_controller.player.movieInfo != null) {
					/* Get the playback info. */
					/* Note that the info becomes valid only when the player status is in the Playing. */
					float time         = GetTime(cri_mana_controller.player); /* usec */
					uint total_frames = cri_mana_controller.player.movieInfo.totalFrames;
					uint framerate    = cri_mana_controller.player.movieInfo.framerateN;
					if (framerate == 0) {
						framerate = 1; // To avoide zero division
					}

                    /* Display the playback info. */
                    this.timestr = string.Format("Time : {0:0.000} sec", time);
                    this.durstr  = string.Format("Duration : {0:0.000} sec", total_frames * 1000.0f / framerate);
                }
			}
		}
	}
	
	void OnGUI() 
	{
		if (Scene_00_SampleList.ShowList == true) {
			return;
		}

		/* Set UI skin. */
		GUI.skin = Scene_00_SampleList.uiSkin;
		
		GUILayout.BeginArea(new Rect(16, 80, 320, 140), "", Scene_00_SampleList.TextStyle);
		GUILayout.Label("Playback Information");
		GUILayout.Label(this.timestr);
		GUILayout.Label(this.durstr);
		GUILayout.EndArea();
	}
}

/* end of file */
